var updateButton = document.getElementById("update-button");

function showInputPath(edit, input, label) {
    edit.style.display = "none";
    input.style.display = "inline-block";
    label.style.display = "none";
    updateButton.style.display = "inline-block";
}

var editName = document.getElementById("edit-name");
var inputName = document.getElementById("input-name");
var labelName = document.getElementById("label-name");
editName.onclick = function () {
    showInputPath(editName, inputName, labelName);
};


var editSurname = document.getElementById("edit-surname");
var inputSurname = document.getElementById("input-surname");
var labelSurname = document.getElementById("label-surname");
editSurname.onclick = function () {
    showInputPath(editSurname, inputSurname, labelSurname)

};

var editLogin = document.getElementById("edit-login");
var inputLogin = document.getElementById("input-login");
var labelLogin = document.getElementById("label-login");
editLogin.onclick = function () {
    showInputPath(editLogin, inputLogin, labelLogin)
};

var editPassword = document.getElementById("edit-password");
var inputPassword = document.getElementById("input-password");
var labelPassword = document.getElementById("label-password");
editPassword.onclick = function () {
    showInputPath(editPassword, inputPassword, labelPassword)
};

var editAdmin = document.getElementById("edit-admin");
var inputAdmin = document.getElementById("input-admin");
var labelAdmin = document.getElementById("label-admin");
editAdmin.onclick = function () {
    showInputPath(editAdmin, inputAdmin, labelAdmin)
};

var editBlock = document.getElementById("edit-block");
var inputBlock = document.getElementById("input-block");
var labelBlock = document.getElementById("label-block");
editBlock.onclick = function () {
    showInputPath(editBlock, inputBlock, labelBlock)

};
var editRating = document.getElementById("edit-rating");
var inputRating = document.getElementById("input-rating");
var labelRating = document.getElementById("label-rating");
editRating.onclick = function () {
    showInputPath(editRating, inputRating, labelRating)
};


var modalButton = document.getElementById("show_modal");
var modal = document.getElementById("modal");
modalButton.onclick = function () {
    modal.top = "60%";
    modal.tansition = "0.4s";
}