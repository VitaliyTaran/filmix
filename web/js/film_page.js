var markValue = document.getElementById("mark-value");
var oneStar = document.getElementById("one-star");
var twoStar = document.getElementById("two-star");
var threeStar = document.getElementById("three-star");
var fourStar = document.getElementById("four-star");
var fiveStar = document.getElementById("five-star");


oneStar.onmouseover = function () {
    oneStar.style.color = "black";
    twoStar.style.color = "rgba(128, 128, 128, 0.7)";
    threeStar.style.color = "rgba(128, 128, 128, 0.7)";
    fourStar.style.color = "rgba(128, 128, 128, 0.7)";
    fiveStar.style.color = "rgba(128, 128, 128, 0.7)";
};
oneStar.onmouseout = function () {
    changeMarkColor(markValue.getAttribute("value"));
};
oneStar.onclick = function () {
    markValue.setAttribute("value", '1');
};


twoStar.onmouseover = function () {
    oneStar.style.color = "black";
    twoStar.style.color = "black";
    threeStar.style.color = "rgba(128, 128, 128, 0.7)";
    fourStar.style.color = "rgba(128, 128, 128, 0.7)";
    fiveStar.style.color = "rgba(128, 128, 128, 0.7)";
};
twoStar.onmouseout = function () {
    changeMarkColor(markValue.getAttribute("value"));
};
twoStar.onclick = function () {
    markValue.setAttribute("value", '2');

};
threeStar.onmouseover = function () {
    oneStar.style.color = "black";
    twoStar.style.color = "black";
    threeStar.style.color = "black";
    fourStar.style.color = "rgba(128, 128, 128, 0.7)";
    fiveStar.style.color = "rgba(128, 128, 128, 0.7)";
};
threeStar.onmouseout = function () {
    changeMarkColor(markValue.getAttribute("value"));
};
threeStar.onclick = function () {
    markValue.setAttribute("value", '3');
};
fourStar.onmouseover = function () {
    oneStar.style.color = "black";
    twoStar.style.color = "black";
    threeStar.style.color = "black";
    fourStar.style.color = "black";
    fiveStar.style.color = "rgba(128, 128, 128, 0.7)";
};
fourStar.onmouseout = function () {
    changeMarkColor(markValue.getAttribute("value"));
};
fourStar.onclick = function () {
    markValue.setAttribute("value", '4');
};
fiveStar.onmouseover = function () {
    oneStar.style.color = "black";
    twoStar.style.color = "black";
    threeStar.style.color = "black";
    fourStar.style.color = "black";
    fiveStar.style.color = "black";
};
fiveStar.onmouseout = function () {
    changeMarkColor(markValue.getAttribute("value"));
};
fiveStar.onclick = function () {
    markValue.setAttribute("value", '5');
};

function changeMarkColor(markValue) {
    switch (markValue) {
        case '0':
            oneStar.style.color = "rgba(128, 128, 128, 0.7)";
            twoStar.style.color = "rgba(128, 128, 128, 0.7)";
            threeStar.style.color = "rgba(128, 128, 128, 0.7)";
            fourStar.style.color = "rgba(128, 128, 128, 0.7)";
            fiveStar.style.color = "rgba(128, 128, 128, 0.7)";
            break;
        case '1':
            oneStar.style.color = "black";
            twoStar.style.color = "rgba(128, 128, 128, 0.7)";
            threeStar.style.color = "rgba(128, 128, 128, 0.7)";
            fourStar.style.color = "rgba(128, 128, 128, 0.7)";
            fiveStar.style.color = "rgba(128, 128, 128, 0.7)";
            break;
        case '2':
            oneStar.style.color = "black";
            twoStar.style.color = "black";
            threeStar.style.color = "rgba(128, 128, 128, 0.7)";
            fourStar.style.color = "rgba(128, 128, 128, 0.7)";
            fiveStar.style.color = "rgba(128, 128, 128, 0.7)";
            break;
        case '3':
            oneStar.style.color = "black";
            twoStar.style.color = "black";
            threeStar.style.color = "black";
            fourStar.style.color = "rgba(128, 128, 128, 0.7)";
            fiveStar.style.color = "rgba(128, 128, 128, 0.7)";
            break;
        case '4':
            oneStar.style.color = "black";
            twoStar.style.color = "black";
            threeStar.style.color = "black";
            fourStar.style.color = "black";
            fiveStar.style.color = "rgba(128, 128, 128, 0.7)";
            break;
        case '5':
            oneStar.style.color = "black";
            twoStar.style.color = "black";
            threeStar.style.color = "black";
            fourStar.style.color = "black";
            fiveStar.style.color = "black";
            break;
    }
}

var updateButton = document.getElementById("update-button");
var removeButton = document.getElementById("remove-button");

function showInputPath(edit, input, label) {
    edit.style.display = "none";
    input.style.display = "inline-block";
    label.style.display = "none";
    updateButton.style.display = "inline-block";
    removeButton.style.display = "block";
}

var editTitle = document.getElementById("edit-title");
var inputTitle = document.getElementById("input-title");
var labelTitle = document.getElementById("label-title");
editTitle.onclick = function () {
    showInputPath(editTitle, inputTitle, labelTitle)
};

var editGenre = document.getElementById("edit-genre");
var inputGenre = document.getElementById("input-genre");
var labelGenre = document.getElementById("label-genre");
editGenre.onclick = function () {
    showInputPath(editGenre, inputGenre, labelGenre)
};

var editCountry = document.getElementById("edit-country");
var inputCountry = document.getElementById("input-country");
var labelCountry = document.getElementById("label-country");
editCountry.onclick = function () {
    showInputPath(editCountry, inputCountry, labelCountry)
};

var editReleaseDate = document.getElementById("edit-releaseDate");
var inputReleaseDate = document.getElementById("input-releaseDate");
var labelReleaseDate = document.getElementById("label-releaseDate");
editReleaseDate.onclick = function () {
    showInputPath(editReleaseDate, inputReleaseDate, labelReleaseDate)
};
var editType = document.getElementById("edit-type");
var inputType = document.getElementById("input-type");
var labelType = document.getElementById("label-type");
editType.onclick = function () {
    showInputPath(editType, inputType, labelType)
};

var editDiscription = document.getElementById("edit-discription");
var inputDiscription = document.getElementById("input-discription");
var labelDiscription = document.getElementById("label-discription");
editDiscription.onclick = function () {
    showInputPath(editDiscription, inputDiscription, labelDiscription)
};

var element = document.getElementById("view-film-artist");
var addArtist = document.getElementById("create-artist");
addArtist.onclick = function () {
    var a = document.createElement("a");
    element.appendChild(a);
};