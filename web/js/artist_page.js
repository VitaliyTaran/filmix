var updateButton = document.getElementById("update-button");
var removeButton = document.getElementById("remove-button");

function showInputPath(edit, input, label) {
    edit.style.display = "none";
    input.style.display = "inline-block";
    label.style.display = "none";
    updateButton.style.display = "inline-block";
    removeButton.style.display = "block";
}

var editName = document.getElementById("edit-name");
var inputName = document.getElementById("input-name");
var labelName = document.getElementById("label-name");
editName.onclick = function () {
    showInputPath(editName, inputName, labelName);
};


var editSurname = document.getElementById("edit-surname");
var inputSurname = document.getElementById("input-surname");
var labelSurname = document.getElementById("label-surname");
editSurname.onclick = function () {
    showInputPath(editSurname, inputSurname, labelSurname)

};

var editBirthday = document.getElementById("edit-birthday");
var inputBirthday = document.getElementById("input-birthday");
var labelBirthday = document.getElementById("label-birthday");
editBirthday.onclick = function () {
    showInputPath(editBirthday, inputBirthday, labelBirthday)
};

var editBiography = document.getElementById("edit-biography");
var inputBiography = document.getElementById("input-biography");
var labelBiography = document.getElementById("label-biography");
editBiography.onclick = function () {
    showInputPath(editBiography, inputBiography, labelBiography)
};