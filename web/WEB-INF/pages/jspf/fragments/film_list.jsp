<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setBundle basename="locale"/>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<c:forEach var="film" items="${requestScope.films}">
    <div class="films">
        <img src="/img/film.png" id="films-img">
        <h5 id="films-title">
            <a href="${pageContext.request.contextPath}/controller?command=film&id=${film.id}">${film.title}</a>
        </h5>
        <h5 id="films-genre">${film.genre}</h5>
        <h5 id="films-release-date"><fmt:message key="locale.release_date"/>: ${film.releaseDate}</h5>
    </div>
</c:forEach>
</body>
</html>
