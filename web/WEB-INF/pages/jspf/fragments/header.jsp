<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="locale"/>
<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Cuprum" rel="stylesheet">
    <title>Tom menu</title>
</head>
<body>
<header>
    <nav class="main-menu">
        <input type="checkbox" name="toggle" id="menu" class="toggleMenu">
        <label for="menu" class="toggleMenu"><i class="fa fa-bars"></i><fmt:message key="locale.menu"/></label>
        <ul>
            <li><a href="/controller?command=film_all"><i class="fa fa-home"></i><fmt:message key="locale.main"/></a>
            </li>
            <li>
                <input type="checkbox" name="toggle" class="toggleSubmenu" id="sub_m1">
                <a href="/controller?command=film_all"><i class="fa fa-film"></i><fmt:message key="locale.play"/></a>
                <label for="sub_m1" class="toggleSubmenu"><i class="fa"></i></label>
                <ul>
                    <li><a href="/controller?command=film_all&type=film"><fmt:message key="locale.film"/></a></li>
                    <li><a href="/controller?command=film_all&type=cartoon"><fmt:message key="locale.cartoon"/></a></li>
                    <li><a href="/controller?command=film_all&type=tv_series"><fmt:message key="locale.tv_series"/></a>
                    </li>
                    <c:if test="${sessionScope.userSession.admin==true}">
                        <li>
                            <input type="checkbox" name="toggle" class="toggleSubmenu" id="sub_m1-1">
                            <a href="#"><fmt:message key="locale.create"/></a>
                            <label for="sub_m1-1" class="toggleSubmenu"><i class="fa"></i></label>
                            <ul>
                                <li><a href="/controller?command=forward&page=create_film"><fmt:message
                                        key="locale.film"/> </a></li>
                                <li><a href="/controller?command=forward&page=create_artist"><fmt:message
                                        key="locale.artist_page"/></a></li>
                            </ul>
                        </li>
                    </c:if>
                </ul>
            </li>
            <li><a href="#"><i class="fa fa-th-list"></i><fmt:message key="locale.news"/></a></li>
            <li><a href="#"><i class="fa fa-envelope-open"></i><fmt:message key="locale.filters"/></a></li>
            <li>
                <form action="controller?command=search" method="post">
                    <input type="text" name="search" id="search-input">
                    <button class="button" type="submit" id="search-button">
                        <i class="fa fa-search" aria-hidden="true"></i><fmt:message key="locale.search"/>
                    </button>
                </form>
            </li>
            <li>
                <input type="checkbox" name="toggle" class="toggleSubmenu" id="sub_m2">
                <a href="#"><i class="fa fa-user-circle"></i><fmt:message key="locale.user"/></a>
                <label for="sub_m2" class="toggleSubmenu"><i class="fa"></i></label>
                <ul>
                    <c:if test="${sessionScope.userSession == null || sessionScope.loginError == true}">
                        <li><a id="show_modal" href="#modal"><fmt:message key="locale.sing_up"/></a></li>
                    </c:if>
                    <c:if test="${sessionScope.userSession != null}">
                        <li><a href="/controller?command=logout"><fmt:message key="locale.sing_out"/></a></li>
                        <li><a href="/controller?command=user_profile&id=${sessionScope.userSession.id}">
                            <fmt:message key="locale.profile"/></a>
                        </li>
                        <c:if test="${sessionScope.userSession.admin==true}">
                            <li>
                                <a href="/controller?command=user_all">
                                    <fmt:message key="locale.user_all"/></a>
                            </li>
                        </c:if>
                        <li><a href="/controller?command=user_reviews"><fmt:message key="locale.my_views"/></a></li>
                    </c:if>
                    <li>
                        <input type="checkbox" name="toggle" class="toggleSubmenu" id="sub_m2-1">
                        <a href="#"><fmt:message key="locale.language"/></a>
                        <label for="sub_m2-1" class="toggleSubmenu"><i class="fa"></i></label>
                        <ul>
                            <li><a href="/controller?command=locale&value=ru_RU">Ru</a></li>
                            <li><a href="/controller?command=locale&value=en_US">En</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</header>
<aside class="modal" id="modal">
    <header>
        <img src="img/user.png">
        <a href="#" class="btn"><i class="fa fa-times"></i></a>
    </header>
    <section>
        <form action="controller?command=login" method="post">
            <div class="modal-input">
                <input type="text" name="login" placeholder="<fmt:message key="locale.enter_login"/>">
            </div>
            <div class="modal-input">
                <input type="password" name="password" placeholder="<fmt:message key="locale.enter_password"/>">
            </div>
            <input id="registration-button-first" class="modal-submit" type="submit" name="submit"
                   value="<fmt:message key="locale.submit"/>"><br/>

        </form>
        <form action="/controller?command=forward&page=create_user" method="post">
            <input id="registration-button-second" class="modal-submit" type="submit" name="submit"
                   value="<fmt:message key="locale.registration"/>"><br/>
        </form>
        <a href="#"><fmt:message key="locale.restore"/></a>
    </section>
</aside>

</body>
</html>
