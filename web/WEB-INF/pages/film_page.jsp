<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setBundle basename="locale"/>
<html>
<head>
    <title>Title</title>
    <meta http-equiv="Content-Language" content="en"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<header>
    <jsp:include page="/WEB-INF/pages/jspf/fragments/header.jsp"/>
</header>
<section>
    <div class="film-info">
        <form action="controller?command=save_film" method="post">
            <div class="film-info-mark">
                <label><fmt:message key="locale.mark"/>:</label>
                <c:choose>
                    <c:when test="${film.avgMark > 0 && film.avgMark <0.5 || film.avgMark == 0}">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                    </c:when>
                    <c:when test="${film.avgMark > 0.5 && film.avgMark <1 || film.avgMark == 0.5}">
                        <a class="star-marked" href="#"><i class="fa fa-star-half"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                    </c:when>
                    <c:when test="${film.avgMark >1 && film.avgMark < 1.5 ||film.avgMark == 1}">
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                    </c:when>
                    <c:when test="${film.avgMark >1.5 && film.avgMark < 2 || film.avgMark == 1.5}">
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star-half"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                    </c:when>
                    <c:when test="${film.avgMark > 2 && film.avgMark < 2.5||film.avgMark == 2}">
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                    </c:when>
                    <c:when test="${film.avgMark > 2.5 && film.avgMark < 3 || film.avgMark == 2.5}">
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star-half"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                    </c:when>
                    <c:when test="${film.avgMark > 3 && film.avgMark < 3.5||film.avgMark == 3}">
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                    </c:when>
                    <c:when test="${film.avgMark > 3.5 && film.avgMark < 4 || film.avgMark == 3.5}">
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star-half"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                    </c:when>
                    <c:when test="${film.avgMark > 4 && film.avgMark < 4.5 || film.avgMark == 4}">
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                    </c:when>
                    <c:when test="${film.avgMark > 4 && film.avgMark < 4.5 || film.avgMark == 4.5}">
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star-half"></i></a>
                    </c:when>
                    <c:when test="${film.avgMark > 4.5 && film.avgMark < 5||film.avgMark == 5}">
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                    </c:when>
                </c:choose>
            </div>
            <h3><fmt:message key="locale.film_page"></fmt:message></h3>
            <div id="films-img">
                <input id="film-info-image" type="file" name="filmImage" accept="image/*">
                <img src="img/film.png">
            </div>

            <input type="number" name="id" value="${film.id}" hidden>

            <div id="view-film-title">
                <fmt:message key="locale.title"/>: <label id="label-title">${film.title} </label>
                <c:if test="${sessionScope.userSession.admin == true}">
                    <a id="edit-title" href="#edit-title"><i class="fa fa-pencil "></i><fmt:message key="locale.edit"/></a>
                    <input id="input-title" type="text" name="title" placeholder="${film.title}" value="${film.title}">
                </c:if>
            </div>

            <div id="view-film-genre">
                <fmt:message key="locale.genre"/>: <label id="label-genre"> ${film.genre}</label>
                <c:if test="${sessionScope.userSession.admin == true}">
                    <a id="edit-genre" href="#edit-genre"><i class="fa fa-pencil"></i><fmt:message
                            key="locale.edit"/></a>
                    <input id="input-genre" type="text" name="genre" placeholder="${film.genre}" value="${film.genre}">
                </c:if>
            </div>

            <div id="view-film-country">
                <fmt:message key="locale.country"/>: <label id="label-country"> ${film.country}</label>
                <c:if test="${sessionScope.userSession.admin == true}">
                    <a id="edit-country" href="#edit-country"><i class="fa fa-pencil "></i><fmt:message
                            key="locale.edit"/></a>
                    <input id="input-country" type="text" name="country" placeholder="${film.country}"
                           value="${film.country}">
                </c:if>
            </div>

            <div id="view-film-release-date">
                <fmt:message key="locale.release_date"/>:
                <label id="label-releaseDate">${film.releaseDate}</label>
                <c:if test="${sessionScope.userSession.admin == true}">
                    <a id="edit-releaseDate" href="#edit-releaseDate"><i class="fa fa-pencil "></i><fmt:message
                            key="locale.edit"/></a>
                    <input type="date" id="input-releaseDate" name="releaseDate" value="${film.releaseDate}">
                </c:if>
            </div>

            <div id="view-film-type">
                <fmt:message key="locale.type"/>:
                <label id="label-type">
                    <c:if test="${film.type == 'CARTOON'}">
                        <fmt:message key="locale.cartoon"/>
                    </c:if>
                    <c:if test="${film.type == 'TV_SERIES'}">
                        <fmt:message key="locale.tv_series"/>
                    </c:if>
                    <c:if test="${film.type == 'FILM'}">
                        <fmt:message key="locale.film"/>
                    </c:if>
                </label>
                <c:if test="${sessionScope.userSession.admin == true}">
                    <a id="edit-type" href="#edit-type"><i class="fa fa-pencil "></i><fmt:message
                            key="locale.edit"/></a>
                    <select id="input-type" name="type" value="${film.type}">
                        <option value="CARTOON"><fmt:message key="locale.cartoon"/></option>
                        <option value="FILM"><fmt:message key="locale.film"/></option>
                        <option value="TV_SERIES"><fmt:message key="locale.tv_series"/></option>
                    </select>
                </c:if>
            </div>

            <div id="view-film-artist">

                Актреский состав
                <c:forEach items="${film.artists}" var="artist">
                    <a id="artist-href"
                       href="/controller?command=artist&id=${artist.id}">${artist.name} ${artist.surname}</a>
                    <c:if test="${sessionScope.userSession.admin==true}">
                        <a id="remove-artist" href="/controller?command=remove_artist_film&filmId=${film.id}&artistId=${artist.id}">
                            <i class="fa fa-minus-circle"></i></a>
                        <br>
                    </c:if>

                </c:forEach>
                <c:if test="${sessionScope.userSession.admin==true}">
                    <a href="#add-artist"><i class="fa fa-plus-circle"></i></a>
                </c:if>
            </div>

            <div id="view-film-discription">
                <fmt:message key="locale.discription"/>: <label id="label-discription">${film.discription}</label>
                <c:if test="${sessionScope.userSession.admin == true}">
                    <a id="edit-discription" href="#edit-discription"><i class="fa fa-pencil "></i><fmt:message
                            key="locale.edit"/></a>
                    <textarea id="input-discription" name="discription" cols="100" rows="auto"
                              placeholder="${film.discription}">${film.discription}</textarea>
                </c:if>
            </div>


            <button class="button" id="update-button" type="submit" hidden><fmt:message
                    key="locale.save"/></button>
        </form>
        <form action="/controller?command=remove_film" method="post">
            <input type="number" name="id" value="${film.id}" hidden>
            <button class="button" id="remove-button" type="submit" hidden><fmt:message
                    key="locale.remove"/></button>
        </form>
    </div>

    <c:if test="${sessionScope.userSession!=null}">
        <div class="add-review">
            <form action="/controller?command=save_review" method="post">

                <input class="hidden-input" name="userId" value="${sessionScope.userSession.id}">
                <input class="hidden-input" name="filmId" value="${film.id}">
                <img class="user-film-img" src="/img/user.png">
                <div class="user-film-name">${sessionScope.userSession.name} ${sessionScope.userSession.surname}</div>

                <div class="user-film-status">
                    <label><fmt:message key="locale.status"/>: </label>
                    <select class="status-value" name="status">
                        <option value="WATCHING"><fmt:message key="locale.watching"/></option>
                        <option value="WATCHED"><fmt:message key="locale.watched"/></option>
                        <option value="DROPPED"><fmt:message key="locale.dropped"/></option>
                        <option value="FAVORITE"><fmt:message key="locale.favorite"/></option>
                    </select>
                </div>
                <div class="user-film-input-review"><textarea name="review" cols="100" rows="20"></textarea></div>
                <div class="user-film-mark">
                    <label><fmt:message key="locale.mark"/>:</label>
                    <input id="mark-value" type="number" name="mark" value="0" hidden>
                    <a id="one-star" href="#one-star"><i class="fa fa-star"></i></a>
                    <a id="two-star" href="#two-star"><i class="fa fa-star"></i></a>
                    <a id="three-star" href="#three-star"><i class="fa fa-star"></i></a>
                    <a id="four-star" href="#four-star"><i class="fa fa-star"></i></a>
                    <a id="five-star" href="#five-star"><i class="fa fa-star"></i></a>
                </div>
                <div class=user-film-local-date>
                    <input class="hidden-input" name="date" type="text" value="<ctg:info-time/>">
                </div>
                <button class="button" type="submit"><fmt:message key="locale.submit"/></button>
            </form>
        </div>
    </c:if>

    <c:forEach var="review" items="${reviews}">
        <div class="review-info">
            <form action="/controller?command=remove_review" method="post">
                <input class="hidden-input" name="id" value="${review.id}">
                <input class="hidden-input" name="filmId" value="${review.film.id}">
                <img class="user-film-img" src="/img/user.png">
                <div class="user-film-name">${review.user.name} ${review.user.surname}</div>
                <div class="user-film-status">
                    <label><fmt:message key="locale.status"/>:
                        <c:if test="${review.status == 'WATCHING'}">
                            <fmt:message key="locale.watching"/>
                        </c:if>
                        <c:if test="${review.status == 'WATCHED'}">
                            <fmt:message key="locale.watched"/>
                        </c:if>
                        <c:if test="${review.status == 'DROPPED'}">
                            <fmt:message key="locale.dropped"/>
                        </c:if>
                        <c:if test="${review.status == 'FAVORITE'}">
                            <fmt:message key="locale.favorite"/>
                        </c:if>
                    </label>
                </div>
                <div class="user-film-input-review">
                    <fmt:message key="locale.discription"/>: <label id="label-discription">${review.discription}</label>
                </div>
                <div class="user-film-mark">
                    <label><fmt:message key="locale.mark"/>:</label>
                    <c:choose>
                        <c:when test="${review.mark == 0}">
                            <a href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                        </c:when>
                        <c:when test="${review.mark == 1}">
                            <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                        </c:when>
                        <c:when test="${review.mark == 2}">
                            <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                            <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                        </c:when>
                        <c:when test="${review.mark == 3}">
                            <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                            <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                            <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                        </c:when>
                        <c:when test="${review.mark == 4}">
                            <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                            <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                            <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                            <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                        </c:when>
                        <c:when test="${review.mark == 5}">
                            <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                            <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                            <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                            <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                            <a class="star-marked" href="#"><i class="fa fa-star"></i></a>
                        </c:when>
                    </c:choose>
                </div>
                <div class=user-film-local-date>
                    <label><fmt:message key="locale.date"/>: ${review.date}</label>
                    <input class="hidden-input" name="date" type="text" value="<ctg:info-time/>"></div>
                <c:if test="${review.user.id == sessionScope.userSession.id}">
                    <button class="button" type="submit"><fmt:message key="locale.remove_comment"/></button>
                </c:if>
            </form>
        </div>
    </c:forEach>
</section>
<aside class="modal" id="add-artist">
    <header>
        <a href="#" class="btn"><i class="fa fa-times"></i></a>
    </header>
    <section>
        <form action="controller?command=add_artist" method="post">
            <input name="filmId" value="${film.id}" hidden>
            <div class="actor-model-label">
                <h3><fmt:message key="locale.adding-actor"/></h3>
            </div>
            <div class="artist-modal-input">
                <select name="artistId">
                    <c:forEach items="${artists}" var="artist">
                        <option value="${artist.id}">${artist.name} ${artist.surname}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="artist-modal-input">
                <input type="text" name="role">
            </div>
            <input id="add-artist-submit-button" class="modal-submit" type="submit" name="submit"
                   value="<fmt:message key="locale.submit"/>"><br/>

        </form>
    </section>
</aside>

<script src="/js/film_page.js"></script>
</body>
</html>