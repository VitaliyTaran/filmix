<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setBundle basename="locale"/>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<header>
    <jsp:include page="/WEB-INF/pages/jspf/fragments/header.jsp"/>
</header>
<section>
    <c:forEach items="${reviews}" var="review">
        <div class="review">
            <div id="review-user-name"><fmt:message
                    key="locale.user"/>: ${review.user.name} ${review.user.surname}</div>
            <img src="img/film.png" id="film-img">
            <div id="review-mark"><fmt:message key="locale.mark"/>: ${review.mark}</div>
            <div id="review-date"><fmt:message key="locale.date"/>: ${review.date}</div>
            <div id="review-film-title"><fmt:message key="locale.film"/>:<a
                href="/controller?command=film&id=${review.film.id}"> ${review.film.title}</a></div>
            <div id="review-comment"><fmt:message key="locale.comment"/>: ${review.discription}</div>
        </div>
    </c:forEach>
</section>
</body>
</html>
