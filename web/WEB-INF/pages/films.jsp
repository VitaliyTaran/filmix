<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setBundle basename="locale"/>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<header>
    <jsp:include page="/WEB-INF/pages/jspf/fragments/header.jsp"/>
</header>
<section>
    <jsp:include page="/WEB-INF/pages/jspf/fragments/film_list.jsp"/>
</section>
</body>
</html>
