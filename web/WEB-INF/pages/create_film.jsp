<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setBundle basename="locale"/>
<html>
<head>
    <title>Title</title>
    <meta http-equiv="Content-Language" content="en"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<header>
    <jsp:include page="/WEB-INF/pages/jspf/fragments/header.jsp"/>
</header>
<section>
    <div class="create-film">
        <form action="controller?command=save_film" method="post">

            <h3><fmt:message key="locale.film_page"></fmt:message></h3>

            <img src="img/film.png" id=film-img>

            <input type="number" name="id" value="${film.id}" hidden>

            <div id="view-film-title">
                <fmt:message key="locale.title"/>:
                <input id="input-title" type="text" name="title">
            </div>

            <div id="view-film-genre">
                <fmt:message key="locale.genre"/>:
                <input id="input-genre" type="text" name="genre">
            </div>

            <div id="view-film-country">
                <fmt:message key="locale.country"/>:
                <input id="input-country" type="text" name="country">
            </div>

            <div id="view-film-release-date">
                <fmt:message key="locale.release_date"/>:
                <input type="date" id="input-releaseDate" name="releaseDate">
            </div>

            <div id="view-film-type">
                <fmt:message key="locale.type"/>:
                <select id="input-type" name="type" value="${film.type}">
                    <option value="CARTOON"><fmt:message key="locale.cartoon"/></option>
                    <option value="FILM"><fmt:message key="locale.film"/></option>
                    <option value="TV_SERIES"><fmt:message key="locale.tv_series"/></option>
                </select>
            </div>

            <div id="view-film-discription">
                <fmt:message key="locale.discription"/>:
                <textarea id="input-discription" name="discription" cols="100" rows="auto"></textarea>
            </div>

            <button class="button" id="update-button" type="submit"><fmt:message
                    key="locale.create"/></button>

        </form>
    </div>
</section>
<script src="/js/film_page.js"></script>
</body>
</html>