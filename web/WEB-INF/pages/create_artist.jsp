<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setBundle basename="locale"/>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<header>
    <jsp:include page="/WEB-INF/pages/jspf/fragments/header.jsp"/>
</header>
<section>
    <div class=create-artist>
        <form action="/controller?command=save_artist" method="post">
            <h3><fmt:message key="locale.create_artist_page"></fmt:message></h3>

            <img src="img/user.png" id=artist-img>

            <input type="number" name="id" value="${artist.id}" hidden>

            <div id="view-artist-name">
                <fmt:message key="locale.name"/>:
                <input id="input-name" type="text" name="name">
            </div>

            <div id="view-artist-surname">
                <fmt:message key="locale.surname"/>:
                <input id="input-surname" type="text" name="surname">
            </div>

            <div id="view-artist-birthday">
                <fmt:message key="locale.birthday"/>:
                <input id="input-birthday" type="date" name="birthday">
            </div>

            <div id="view-artist-biography">
                <fmt:message key="locale.biography"/>:
                <textarea name="biography" cols="100" rows="auto"> </textarea>
            </div>

            <button class="button" id="update-button" type="submit"><fmt:message
                    key="locale.create"/></button>
        </form>
    </div>
</section>
</body>
</html>
