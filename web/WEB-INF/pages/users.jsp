<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setBundle basename="locale"/>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<header>
    <jsp:include page="/WEB-INF/pages/jspf/fragments/header.jsp"/>
</header>
<section>
    <c:forEach var="user" items="${requestScope.users}">
        <div class="user-list">
            <img src="/img/user.png" id="films-img">
            <h5 id="user-login">
                <a href="${pageContext.request.contextPath}/controller?command=user_profile&id=${user.id}">${user.login}</a>
            </h5>
            <h5 id="user-name-surname">${user.name} ${user.surname}</h5>
            <h5 id="user-rating"><fmt:message key="locale.rating"/> ${user.rating}</h5>
            <h5 id="user-status"><fmt:message key="locale.status"/> <c:if test="${user.blocked}">
                <fmt:message key="locale.block"/>
            </c:if>
                <c:if test="${!user.blocked}">
                    <fmt:message key="locale.free"/>
                </c:if>
            </h5>
        </div>
    </c:forEach>
</section>
</body>
</html>
