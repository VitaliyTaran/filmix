<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setBundle basename="locale"/>
<html>
<head>
    <title>User Page</title>
    <meta http-equiv="Content-Language" content="en"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<header>
    <jsp:include page="/WEB-INF/pages/jspf/fragments/header.jsp"/>
</header>
<nav></nav>
<sidebar></sidebar>
<section>
    <div class="create-user">
        <form action="controller?command=save_user_profile" method="post">

            <h3><fmt:message key="locale.create_user"></fmt:message></h3>
            <img src="img/user.png" id="user-image">

            <input type="number" name="id" value="" hidden>
            <ul id="user-profile-list">
                <li>
                    <fmt:message key="locale.name"/>
                    <input id="input-name" type="text" name="name">
                </li>

                <li>
                    <fmt:message key="locale.surname"/>
                    <input id="input-surname" type="text" name="surname">
                </li>

                <li>
                    <fmt:message key="locale.login"/>
                    <input id="input-login" type="text" name="login">
                </li>

                <li>
                    <fmt:message key="locale.password"/>
                    <input type="text" name="password">
                </li>
                <li>
                    <fmt:message key="locale.check_password"/>
                    <input type="text" name="checkPassword">
                </li

                <li>
                    <c:if test="${sessionScope.userSession.admin==true}">
                        <fmt:message key="locale.role"/>
                        <select id="input-admin" name="admin">
                            <option value="false"><fmt:message key="locale.user"/></option>
                            <option value="true"><fmt:message key="locale.admin"/></option>
                        </select>
                    </c:if>
                    <c:if test="${sessionScope.userSession==null}">
                        <input name="admin" value="false" hidden>
                    </c:if>
                </li>

                <li>
                    <input name="block" value="false" hidden>
                </li>

                <li>
                    <c:if test="${sessionScope.userSession.admin==true}">
                        <fmt:message key="locale.rating"/>
                        <input id="input-rating" type="number" name="rating" value="100">
                    </c:if>
                    <c:if test="${sessionScope.userSession==null}">
                        <input type="number" name="rating" value="100" hidden>
                    </c:if>
                </li>
                <button class="button" id="update-button" type="submit"><fmt:message
                        key="locale.save"/></button>
            </ul>

        </form>
    </div>
</section>
<script src="/js/user_page.js"></script>
</body>
</html>
