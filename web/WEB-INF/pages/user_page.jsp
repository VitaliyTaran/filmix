<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setBundle basename="locale"/>
<html>
<head>
    <title>User Page</title>
    <meta http-equiv="Content-Language" content="en"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<header>
    <jsp:include page="/WEB-INF/pages/jspf/fragments/header.jsp"/>
</header>
<nav></nav>
<sidebar></sidebar>
<section>
    <div class="user">
        <form action="controller?command=save_user_profile" method="post">

            <h3><fmt:message key="locale.user_profile"></fmt:message></h3>
            <img src="img/user.png" id="user-image">

            <input type="number" name="id" value="${user.id}" hidden>
            <ul id="user-profile-list">
                <li>
                    <fmt:message key="locale.name"/> <label id="label-name">${user.name} </label>
                    <a id="edit-name" href="#"><i class="fa fa-pencil "></i><fmt:message key="locale.edit"/></a>
                    <input id="input-name" type="text" name="name" placeholder="${user.name}" value="${user.name}">
                </li>

                <li>
                    <fmt:message key="locale.surname"/> <label id="label-surname">${user.surname}</label>
                    <a id="edit-surname" href="#"><i class="fa fa-pencil "></i><fmt:message key="locale.edit"/></a>
                    <input id="input-surname" type="text" name="surname" placeholder="${user.surname}"
                           value="${user.surname}">
                </li>

                <li>
                    <fmt:message key="locale.login"/><label id="label-login"> ${user.login}</label>
                    <a id="edit-login" href="#"><i class="fa fa-pencil"></i><fmt:message key="locale.edit"/></a>
                    <input id="input-login" type="text" name="login" placeholder="${user.login}" value="${user.login}">
                </li>

                <li>
                    <fmt:message key="locale.password"/> <label id="label-password"> ${user.password}</label>
                    <a id="edit-password" href="#"><i class="fa fa-pencil "></i><fmt:message key="locale.edit"/></a>
                    <input id="input-password" type="text" name="password" placeholder="${user.password}"
                           value="${user.password}">

                </li>
                <c:if test="${sessionScope.userSession.admin==true}">
                    <li>
                        <fmt:message key="locale.role"/>
                        <label id="label-admin">
                            <c:if test="${user.admin}">
                                <fmt:message key="locale.admin"/>
                            </c:if>

                            <c:if test="${!user.admin}">
                                <fmt:message key="locale.user"/>
                            </c:if>
                        </label>
                        <a id="edit-admin" href="#"><i class="fa fa-pencil "></i><fmt:message key="locale.edit"/></a>
                        <select id="input-admin" name="admin" value="${user.admin}">
                            <option value="false"><fmt:message key="locale.user"/></option>
                            <option value="true"><fmt:message key="locale.admin"/></option>
                        </select>
                    </li>
                </c:if>

                <li>
                    <fmt:message key="locale.status"/>
                    <label id="label-block">
                        <c:if test="${user.blocked}">
                            <fmt:message key="locale.block"/>
                        </c:if>

                        <c:if test="${!user.blocked}">
                            <fmt:message key="locale.free"/>
                        </c:if>
                    </label>
                    <c:if test="${sessionScope.userSession.admin==true}">
                        <a id="edit-block" href="#"><i class="fa fa-pencil "></i><fmt:message key="locale.edit"/></a>
                        <select id="input-block" name="block" value="${user.blocked}">
                            <option value="false"><fmt:message key="locale.free"/></option>
                            <option value="true"><fmt:message key="locale.block"/></option>
                        </select>
                    </c:if>
                </li>

                <li>
                    <fmt:message key="locale.rating"/> <label id="label-rating"> ${user.rating}</label>
                    <c:if test="${sessionScope.userSession.admin==true}">
                        <a id="edit-rating" href="#"><i class="fa fa-pencil"></i><fmt:message key="locale.edit"/></a>
                        <input id="input-rating" type="number" name="rating" placeholder="${user.rating}"
                               value="${user.rating}">
                    </c:if>
                </li>
                <button class="button" id="update-button" type="submit" hidden><fmt:message
                        key="locale.save"/></button>
            </ul>

        </form>
    </div>
</section>
<script src="/js/user_page.js"></script>
</body>
</html>
