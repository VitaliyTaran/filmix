<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setBundle basename="locale"/>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<header>
    <jsp:include page="/WEB-INF/pages/jspf/fragments/header.jsp"/>
</header>
<section>
    <div class="artist">
        <form action="controller?command=save_artist" method="post">
            <h3><fmt:message key="locale.artist_page"></fmt:message></h3>
            <img src="img/user.png" id="user-image">

            <input type="number" name="id" value="${artist.id}" hidden>
            <div id="view-artist-name">
                <fmt:message key="locale.name"/> <label id="label-name">${artist.name} </label>
                <c:if test="${sessionScope.userSession.admin==true}">
                    <a id="edit-name" href="#"><i class="fa fa-pencil "></i><fmt:message key="locale.edit"/></a>
                    <input id="input-name" type="text" name="name" placeholder="${artist.name}" value="${artist.name}">
                </c:if>

            </div>
            <div id="view-artist-surname">
                <fmt:message key="locale.surname"/> <label id="label-surname">${artist.surname}</label>
                <c:if test="${sessionScope.userSession.admin==true}">
                    <a id="edit-surname" href="#"><i class="fa fa-pencil "></i><fmt:message key="locale.edit"/></a>
                    <input id="input-surname" type="text" name="surname" placeholder="${artist.surname}"
                           value="${artist.surname}">
                </c:if>
            </div>

            <div id="view-artist-birthday">
                <fmt:message key="locale.birthday"/>:<label id="label-birthday">${artist.birthday}</label>
                <c:if test="${sessionScope.userSession.admin==true}">
                    <a id="edit-birthday" href="#"><i class="fa fa-pencil "></i><fmt:message key="locale.edit"/></a>
                    <input id="input-birthday" type="date" name="birthday" value="${artist.birthday}">
                </c:if>
            </div>

            <div id="view-artist-biography">
                <fmt:message key="locale.biography"/>: <label id="label-biography">${artist.biography}</label>
                <c:if test="${sessionScope.userSession.admin==true}">
                    <a id="edit-biography" href="#"><i class="fa fa-pencil "></i><fmt:message
                            key="locale.edit"/></a>
                    <textarea id="input-biography" name="biography" cols="100" rows="auto"
                              placeholder="${artist.biography}" value="${artist.biography}"></textarea>
                </c:if>
            </div>

            <button class="button" id="update-button" type="submit" hidden><fmt:message
                    key="locale.save"/></button>

        </form>
        <form action="/controller?command=remove_artist" method="post">
            <input type="number" name="id" value="${artist.id}" hidden>
            <button class="button" id="remove-button" type="submit" hidden><fmt:message
                    key="locale.remove"/></button>
        </form>
    </div>
</section>
<script src="/js/artist_page.js"></script>
</body>
</html>
