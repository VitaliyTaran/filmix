package com.epam.filmix.builder.impl;

import com.epam.filmix.builder.Builder;
import com.epam.filmix.builder.constant.BuilderConstant;
import com.epam.filmix.entity.Film;
import com.epam.filmix.entity.Review;
import com.epam.filmix.entity.User;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ReviewBuilder implements Builder<Review> {
    @Override
    public Review build(ResultSet resultSet) throws SQLException {
        Review result = new Review();
        Film film = new Film();
        User user = new User();

        Date date = resultSet.getDate(BuilderConstant.REVIEW_DATE);
        film.setId(resultSet.getLong(BuilderConstant.REVIEW_FILM_ID));
        user.setId(resultSet.getLong(BuilderConstant.REVIEW_USER_ID));
        long reviewId = resultSet.getLong(BuilderConstant.REVIEW_ID);
        double reviewMark = resultSet.getDouble(BuilderConstant.REVIEW_MARK);
        String reviewStatus = resultSet.getString(BuilderConstant.REVIEW_STATUS);
        String reviewDiscription = resultSet.getString(BuilderConstant.REVIEW_REVIEW);

        result.setDate(date.toLocalDate());
        result.setFilm(film);
        result.setUser(user);
        result.setId(reviewId);
        result.setDiscription(reviewDiscription);
        result.setMark(reviewMark);
        result.setStatus(reviewStatus);
        return result;
    }
}
