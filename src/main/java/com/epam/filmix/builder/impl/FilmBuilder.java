package com.epam.filmix.builder.impl;

import com.epam.filmix.builder.Builder;
import com.epam.filmix.builder.constant.BuilderConstant;
import com.epam.filmix.entity.Film;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class FilmBuilder implements Builder<Film> {
    @Override
    public Film build(ResultSet resultSet) throws SQLException {
        Date date = resultSet.getDate(BuilderConstant.FILM_RELEASE_DATE);
        return new Film(resultSet.getLong(BuilderConstant.FILM_ID),
                resultSet.getString(BuilderConstant.FILM_TITLE),
                resultSet.getString(BuilderConstant.FILM_DISCRIPTION), resultSet.getString(BuilderConstant.FILM_GENRE),
                resultSet.getString(BuilderConstant.FILM_COUNTRY),date.toLocalDate() ,
                resultSet.getString(BuilderConstant.FILM_TYPE));
    }
}
