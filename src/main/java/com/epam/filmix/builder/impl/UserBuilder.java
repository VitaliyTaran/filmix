package com.epam.filmix.builder.impl;

import com.epam.filmix.builder.Builder;
import com.epam.filmix.builder.constant.BuilderConstant;
import com.epam.filmix.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserBuilder implements Builder<User> {
    @Override
    public User build(ResultSet resultSet) throws SQLException {
        return new User(resultSet.getLong(BuilderConstant.USER_ID), resultSet.getString(BuilderConstant.USER_LOGIN),
                resultSet.getString(BuilderConstant.USER_PASSWORD), resultSet.getString(BuilderConstant.USER_NAME),
                resultSet.getString(BuilderConstant.USER_SURNAME), resultSet.getBoolean(BuilderConstant.USER_IS_ADMIN),
                resultSet.getInt(BuilderConstant.USER_RATING), resultSet.getBoolean(BuilderConstant.USER_IS_BLOCKED));
    }
}
