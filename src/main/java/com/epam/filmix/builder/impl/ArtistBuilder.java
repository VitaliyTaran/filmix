package com.epam.filmix.builder.impl;

import com.epam.filmix.builder.Builder;
import com.epam.filmix.builder.constant.BuilderConstant;
import com.epam.filmix.entity.Artist;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ArtistBuilder implements Builder<Artist> {
    @Override
    public Artist build(ResultSet resultSet) throws SQLException {
        Date date = resultSet.getDate(BuilderConstant.ARTIST_BIRTHDAY);
        return new Artist(resultSet.getLong(BuilderConstant.ARTIST_ID), resultSet.getString(BuilderConstant.ARTIST_NAME),
                resultSet.getString(BuilderConstant.ARTIST_SURNAME), date.toLocalDate(), resultSet.getString(BuilderConstant.ARTIST_BIOGRAPHY));
    }
}
