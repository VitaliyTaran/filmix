package com.epam.filmix.builder.constant;

public class BuilderConstant {

    public static final String ARTIST_ID = "artist_id";
    public static final String ARTIST_NAME = "artist_name";
    public static final String ARTIST_SURNAME = "artist_surname";
    public static final String ARTIST_BIRTHDAY = "artist_birthday";
    public static final String ARTIST_ROLE = "artist_role";
    public static final String ARTIST_BIOGRAPHY = "artist_biography";

    public static final String REVIEW_ID = "review_id";
    public static final String REVIEW_REVIEW = "review_review";
    public static final String REVIEW_MARK = "review_mark";
    public static final String REVIEW_DATE = "review_date";
    public static final String REVIEW_STATUS = "review_status";
    public static final String REVIEW_USER_ID = "review_user_id";
    public static final String REVIEW_FILM_ID = "review_film_id";

    public static final String FILM_ID = "film_id";
    public static final String FILM_TITLE = "film_title";
    public static final String FILM_DISCRIPTION = "film_discription";
    public static final String FILM_GENRE = "film_genre";
    public static final String FILM_COUNTRY = "film_country";
    public static final String FILM_RELEASE_DATE = "film_release_date";
    public static final String FILM_TYPE = "film_type";

    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_SURNAME = "user_surname";
    public static final String USER_LOGIN = "user_login";
    public static final String USER_PASSWORD = "user_password";
    public static final String USER_IS_ADMIN = "user_is_admin";
    public static final String USER_RATING = "user_rating";
    public static final String USER_IS_BLOCKED = "user_is_blocked";
}
