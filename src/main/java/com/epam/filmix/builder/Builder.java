package com.epam.filmix.builder;

import com.epam.filmix.entity.Identifable;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Builder<T extends Identifable> {
    T build(ResultSet resultSet) throws SQLException;

}
