package com.epam.filmix.validator;

import com.epam.filmix.logic.exception.LogicFilmixException;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PositiveLongValidator {
    private static final Pattern POSITIVE_LONG_PATTERN = Pattern.compile("\\d+");
    private static final String INCORRECT_LONG_NUMBER = " is incorrect long value";

    public static boolean validate(String value) {
        if (value == null || value.isEmpty()) {
            return false;
        }

        Matcher matcher = POSITIVE_LONG_PATTERN.matcher(value);
        if (matcher.matches()) {
            BigDecimal bigDecimal = new BigDecimal(value);
            return bigDecimal.compareTo(new BigDecimal(Integer.MAX_VALUE)) != 1;
        }
        return false;
    }

    public static long parseStringToLong(String value) throws LogicFilmixException {
        if (validate(value)) {
            return Long.parseLong(value);
        } else {
            throw new LogicFilmixException(INCORRECT_LONG_NUMBER);
        }
    }

}