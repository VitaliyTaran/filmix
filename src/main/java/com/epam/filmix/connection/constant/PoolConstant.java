package com.epam.filmix.connection.constant;


/**
 * The Class PoolConstant.
 */
public class PoolConstant {
    
    /** The Constant PROPERTY_PATH. */
    public static final String BD_PATH = "jdbc:mysql://localhost:3306/film_rating?autoReconnect=true&useSSL=false&characterEncoding=UTF-8";
    
    /** The Constant PROPERTY_USERNAME. */
    public static final String BD_USERNAME = "root";
    
    /** The Constant PROPERTY_PASSWORD. */
    public static final String BD_PASSWORD = "root";

    /** The Constant CONNECTION_SIZE_IS_NULL_MESSAGE. */
    public static final String CONNECTION_SIZE_IS_NULL_MESSAGE = "Connection size = 0.";
    
    /** The Constant CONNECTION_IS_NOT_IN_BUSY_MESSAGE. */
    public static final String CONNECTION_IS_NOT_IN_BUSY_MESSAGE = "Connection isn't in the busyConnection.";
    
    /** The Constant CONNECTION_NO_CLOSE_MESSAGE. */
    public static final String CONNECTION_NO_CLOSE_MESSAGE = "Can't close connection";
    
    /** The Constant OPEN_PROPERTY_ERROR_MESSAGE. */
    public static final String OPEN_PROPERTY_ERROR_MESSAGE = "Can't open properties to db.";
    
    /** The Constant NULL_VALUE_MESSAGE. */
    public static final String NULL_VALUE_MESSAGE = "Can't create null pier.";
    
    /** The Constant DRIVER_REGISTER_ERROR_MESSAGE. */
    public static final String DRIVER_REGISTER_ERROR_MESSAGE = "Sql driver isn't register.";
}
