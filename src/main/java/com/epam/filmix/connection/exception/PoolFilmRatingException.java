package com.epam.filmix.connection.exception;


public class PoolFilmRatingException extends Exception {

    public PoolFilmRatingException(String message) {
        super(message);
    }

    public PoolFilmRatingException(Throwable cause) {
        super(cause);
    }
}
