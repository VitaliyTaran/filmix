package com.epam.filmix.controller;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.CommandFactory;
import com.epam.filmix.command.constant.PageEnum;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.connection.exception.PoolFilmRatingException;
import com.epam.filmix.connection.pool.ConnectionPool;
import com.epam.filmix.controller.constant.ControllerConstant;
import com.epam.filmix.logic.content.CommandResult;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Controller extends HttpServlet {
    private static final Logger LOGGER = LogManager.getRootLogger();
    private static final String ERROR_MESSAGE_NAME ="errorMessage";

    @Override
    public void init() throws ServletException {
        try {
            ConnectionPool.getInstance().initializeConnectionPool(3, ControllerConstant.DB_URL);
        } catch (PoolFilmRatingException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }


    @Override
    public void destroy() {
        try {
            ConnectionPool.getInstance().closeConnections();
        } catch (PoolFilmRatingException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Command command = CommandFactory.getInstance().initCommand(request);
            CommandResult commandResult = command.execute(request);
            if (commandResult.getResponseType() == CommandResult.ResponseType.FORWARD) {
                request.getRequestDispatcher(commandResult.getPage()).forward(request, response);
            } else {
                response.sendRedirect(commandResult.getPage());
            }
        } catch (CommandFilmixException e) {
            LOGGER.log(Level.ERROR, e);
            request.setAttribute(ERROR_MESSAGE_NAME, e.getMessage());
            request.getRequestDispatcher(PageEnum.ERROR_PAGE.getPagePath()).forward(request, response);
        }
    }
}
// TODO: 26.12.2018 все exceptions пробрасывать до уровня контроллера где логировать(Ps итого логгирование в одном месте)
