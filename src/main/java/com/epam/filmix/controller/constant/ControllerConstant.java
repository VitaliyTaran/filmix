package com.epam.filmix.controller.constant;

public class ControllerConstant {

    public static final String DB_URL = "jdbc:mysql://localhost:3306/filmix?verifyServerCertificate=false&" +
            "useSSL=false&requireSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC";

    public static final String INDEX_PAGE_PATH = "/index.jsp";


    public static final String INDEX_PATH = "INDEX_PATH";

}
