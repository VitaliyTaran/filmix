package com.epam.filmix.util;

import com.epam.filmix.entity.Identifable;
import com.epam.filmix.logic.constant.LogicConstant;
import com.epam.filmix.logic.exception.LogicFilmixException;

import java.util.Optional;

public class OptionalExtractor {

    public static Identifable extractEntityFromOptional(Optional<? extends Identifable> entity) throws LogicFilmixException {
        if (entity.isPresent()) {
            return entity.get();
        } else {
            throw new LogicFilmixException(LogicConstant.ENTITY_NOT_EXIST_MESSAGE);
        }
    }

    public static Identifable extractEntityFromOptional(Optional<? extends Identifable> entity, long id) throws LogicFilmixException {
        if (entity.isPresent()) {
            return entity.get();
        } else {
            throw new LogicFilmixException(LogicConstant.ENTITY_NOT_EXIST_WITH_ID_MESSAGE + id);
        }
    }
}
