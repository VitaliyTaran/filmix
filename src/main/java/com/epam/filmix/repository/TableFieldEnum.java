package com.epam.filmix.repository;

import com.epam.filmix.builder.constant.BuilderConstant;

public enum TableFieldEnum {
    USER("user", "user.id as " + BuilderConstant.USER_ID + ", " +
            "user.name as " + BuilderConstant.USER_NAME + ", " +
            "user.surname as " + BuilderConstant.USER_SURNAME + ", " +
            "user.login as " + BuilderConstant.USER_LOGIN + ", " +
            "user.password as " + BuilderConstant.USER_PASSWORD + ", " +
            "user.is_admin as " + BuilderConstant.USER_IS_ADMIN + ", " +
            "user.rating as " + BuilderConstant.USER_RATING + ", " +
            "user.is_blocked as " + BuilderConstant.USER_IS_BLOCKED),

    REVIEW("film_user", "film_user.id as " + BuilderConstant.REVIEW_ID + ", " +
            "film_user.user_id as " + BuilderConstant.REVIEW_USER_ID + ", " +
            "film_user.film_id as " + BuilderConstant.REVIEW_FILM_ID + ", " +
            "film_user.review as " + BuilderConstant.REVIEW_REVIEW + ", " +
            "film_user.mark as " + BuilderConstant.REVIEW_MARK + ", " +
            "film_user.data_time as " + BuilderConstant.REVIEW_DATE + "," +
            "film_user.status as " + BuilderConstant.REVIEW_STATUS),

    FILM("film", "film.id as " + BuilderConstant.FILM_ID + ", " +
            "film.title as " + BuilderConstant.FILM_TITLE + ", " +
            "film.discription as " + BuilderConstant.FILM_DISCRIPTION + "," +
            "film.genre as " + BuilderConstant.FILM_GENRE + ", " +
            "film.country as " + BuilderConstant.FILM_COUNTRY + ", " +
            "film.release_date as " + BuilderConstant.FILM_RELEASE_DATE + ", " +
            "film.type as " + BuilderConstant.FILM_TYPE),

    ARTIST_FILM("artist_film", "artist_film.film_id as " + BuilderConstant.FILM_ID + ", " +
            "artist_film.artist_id as " + BuilderConstant.ARTIST_ID + ", " +
            "artist_film.role as " + BuilderConstant.ARTIST_ROLE),

    ARTIST("artist", "artist.id as " + BuilderConstant.ARTIST_ID + ", " +
            "artist.name as " + BuilderConstant.ARTIST_NAME + ", " +
            "artist.surname as " + BuilderConstant.ARTIST_SURNAME + ", " +
            "artist.birthday as " + BuilderConstant.ARTIST_BIRTHDAY + ", " +
            "artist.biography as " + BuilderConstant.ARTIST_BIOGRAPHY);

    private String tableName;
    private String tableFields;

    TableFieldEnum(String tableName, String tableFields) {
        this.tableName = tableName;
        this.tableFields = tableFields;
    }

    public String getTableName() {
        return tableName;
    }

    public String getTableFields() {
        return tableFields;
    }
}
