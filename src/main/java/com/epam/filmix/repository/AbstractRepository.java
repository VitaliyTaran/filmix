package com.epam.filmix.repository;


import com.epam.filmix.builder.Builder;
import com.epam.filmix.connection.exception.PoolFilmRatingException;
import com.epam.filmix.connection.pool.ConnectionPool;
import com.epam.filmix.entity.Identifable;
import com.epam.filmix.repository.exception.RepositoryFilmRatingException;
import com.epam.filmix.repository.specification.Specification;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<T extends Identifable> {
    private static final String CLASS_LONG = "java.lang.Long";
    private static final String CLASS_INTEGER = "java.lang.Integer";
    private static final String CLASS_STRING = "java.lang.String";
    private static final String CLASS_BOOLEAN = "java.lang.Boolean";
    private static final String CLASS_DOUBLE = "java.lang.Double";
    private static final String CLASS_DATE = "java.time.LocalDate";
    private static final String SQL_COMMAND_CREATE = "INSERT INTO";
    private static final String SQL_COMMAND_UPDATE = "UPDATE";
    private static final String SQL_COMMAND_SET = "SET";
    private static final String DELETE = "DELETE";
    private static final String SELECT = "SELECT";
    private static final String FROM = "FROM";
    private static final String SPACE = " ";
    private static final String END_SQL_QUERY = ";";


    public Boolean save(String sqlTableName, Specification<T> specification) throws RepositoryFilmRatingException {
        return executeQuery(SQL_COMMAND_CREATE + SPACE + sqlTableName + SPACE, specification);
    }

    public List<T> queryForListResult(String sqlTableName, String fields, Builder<T> builder, Specification<T> specification) throws RepositoryFilmRatingException {
        List<T> result = new ArrayList<>();
        try (Connection connection = ConnectionPool.getInstance().acquireConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT + SPACE + fields + SPACE + FROM + SPACE + sqlTableName + SPACE + specification.toSQL() + END_SQL_QUERY)) {
            PreparedStatement preparedStatement = iterateBySpecificationParameter(statement, specification);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                T entity = builder.build(resultSet);
                result.add(entity);
            }
        } catch (SQLException | PoolFilmRatingException e) {
            throw new RepositoryFilmRatingException(e);
        }
        return result;
    }

    public Optional<T> queryForSingleResult(String sqlTableName, String fields, Builder<T> builder, Specification<T> specification) throws RepositoryFilmRatingException {
        try (Connection connection = ConnectionPool.getInstance().acquireConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT + SPACE + fields + SPACE + FROM + SPACE + sqlTableName + SPACE + specification.toSQL() + END_SQL_QUERY)) {
            PreparedStatement preparedStatement = iterateBySpecificationParameter(statement, specification);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                return Optional.ofNullable(builder.build(resultSet));
            }
            return Optional.empty();
        } catch (SQLException | PoolFilmRatingException e) {
            throw new RepositoryFilmRatingException(e);
        }
    }


    public Boolean update(String sqlTableName, Specification<T> specification) throws RepositoryFilmRatingException {
        return executeQuery(SQL_COMMAND_UPDATE + SPACE + sqlTableName + SPACE + SQL_COMMAND_SET + SPACE, specification);
    }


    public Boolean remove(String sqlTableName, Specification<T> specification) throws RepositoryFilmRatingException {
        return executeQuery(DELETE + SPACE + FROM + SPACE + sqlTableName + SPACE, specification);
    }

    private Boolean executeQuery(String firstSqlQueryPath, Specification<T> specification) throws RepositoryFilmRatingException {
        try (Connection connection = ConnectionPool.getInstance().acquireConnection();
             PreparedStatement statement = connection.prepareStatement(firstSqlQueryPath + specification.toSQL() + END_SQL_QUERY)) {
            PreparedStatement deleteStatement = iterateBySpecificationParameter(statement, specification);
            return deleteStatement.execute();
        } catch (SQLException | PoolFilmRatingException e) {
            throw new RepositoryFilmRatingException(e);
        }
    }

    private PreparedStatement iterateBySpecificationParameter(PreparedStatement statement, Specification<T> specification) throws SQLException {
        List<Object> parameters = specification.parameters();
        for (int i = 0; i < parameters.size(); i++)
            switch (parameters.get(i).getClass().getName()) {
                case CLASS_INTEGER:
                    statement.setLong(i + 1, (Integer) parameters.get(i));
                    break;
                case CLASS_LONG:
                    statement.setLong(i + 1, (Long) parameters.get(i));
                    break;
                case CLASS_STRING:
                    statement.setString(i + 1, (String) parameters.get(i));
                    break;
                case CLASS_BOOLEAN:
                    statement.setBoolean(i + 1, (Boolean) parameters.get(i));
                    break;
                case CLASS_DOUBLE:
                    statement.setDouble(i + 1, (Double) parameters.get(i));
                    break;
                case CLASS_DATE:
                    statement.setDate(i + 1, Date.valueOf((java.time.LocalDate) parameters.get(i)));
                    break;
            }
        return statement;
    }

}
