package com.epam.filmix.repository.exception;

public class RepositoryFilmRatingException extends Exception{
    public RepositoryFilmRatingException() {
        super();
    }

    public RepositoryFilmRatingException(String message) {
        super(message);
    }

    public RepositoryFilmRatingException(String message, Throwable cause) {
        super(message, cause);
    }

    public RepositoryFilmRatingException(Throwable cause) {
        super(cause);
    }
}
