package com.epam.filmix.repository.impl;

import com.epam.filmix.builder.Builder;
import com.epam.filmix.builder.impl.RoleBuilder;
import com.epam.filmix.entity.Role;
import com.epam.filmix.repository.AbstractRepository;
import com.epam.filmix.repository.TableFieldEnum;
import com.epam.filmix.repository.exception.RepositoryFilmRatingException;
import com.epam.filmix.repository.specification.Specification;

import java.util.List;
import java.util.Optional;

public class RoleRepository extends AbstractRepository<Role> {
    private TableFieldEnum tableEnum = TableFieldEnum.ARTIST_FILM;
    private Builder<Role> roleBuilder = new RoleBuilder();


    public Boolean save(Specification<Role> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        return super.save(tableName, specification);
    }

    public List<Role> queryForListResult(Specification<Role> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        String tableFields = tableEnum.getTableFields();
        return super.queryForListResult(tableName, tableFields, roleBuilder, specification);
    }

    public Optional<Role> queryForSingleResult(Specification<Role> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        String tableFields = tableEnum.getTableFields();
        return super.queryForSingleResult(tableName, tableFields, roleBuilder, specification);
    }


    public Boolean update(Specification<Role> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        return super.update(tableName, specification);
    }


    public Boolean remove(Specification<Role> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        return super.remove(tableName, specification);
    }
}
