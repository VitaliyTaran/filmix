package com.epam.filmix.repository.impl;

import com.epam.filmix.builder.impl.FilmBuilder;
import com.epam.filmix.entity.Film;
import com.epam.filmix.repository.AbstractRepository;
import com.epam.filmix.repository.TableFieldEnum;
import com.epam.filmix.repository.exception.RepositoryFilmRatingException;
import com.epam.filmix.repository.specification.Specification;

import java.util.List;
import java.util.Optional;

public class FilmRepository extends AbstractRepository<Film> {
    private static final TableFieldEnum tableEnum = TableFieldEnum.FILM;
    private FilmBuilder filmBuilder = new FilmBuilder();

    public Boolean save(Specification<Film> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        return super.save(tableName, specification);
    }

    public List<Film> queryForListResult(Specification<Film> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        String tableFields = tableEnum.getTableFields();
        return super.queryForListResult(tableName, tableFields, filmBuilder, specification);
    }

    public Optional<Film> queryForSingleResult(Specification<Film> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        String tableFields = tableEnum.getTableFields();
        return super.queryForSingleResult(tableName, tableFields, filmBuilder, specification);
    }


    public Boolean update(Specification<Film> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        return super.update(tableName, specification);
    }


    public Boolean remove(Specification<Film> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        return super.remove(tableName, specification);
    }
}
