package com.epam.filmix.repository.impl;


import com.epam.filmix.builder.impl.UserBuilder;
import com.epam.filmix.entity.User;
import com.epam.filmix.repository.AbstractRepository;
import com.epam.filmix.repository.TableFieldEnum;
import com.epam.filmix.repository.exception.RepositoryFilmRatingException;
import com.epam.filmix.repository.specification.Specification;

import java.util.List;
import java.util.Optional;

public class UserRepository extends AbstractRepository<User> {
    private static final TableFieldEnum tableEnum = TableFieldEnum.USER;
    private UserBuilder userBuilder = new UserBuilder();


    public Boolean save(Specification<User> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        return super.save(tableName, specification);
    }

    public List<User> queryForListResult(Specification<User> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        String tableFields = tableEnum.getTableFields();
        return super.queryForListResult(tableName, tableFields, userBuilder, specification);
    }

    public Optional<User> queryForSingleResult(Specification<User> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        String tableFields = tableEnum.getTableFields();
        return super.queryForSingleResult(tableName, tableFields, userBuilder, specification);
    }


    public Boolean update(Specification<User> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        return super.update(tableName, specification);
    }


    public Boolean remove(Specification<User> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        return super.remove(tableName, specification);
    }
}
