package com.epam.filmix.repository.impl;

import com.epam.filmix.builder.Builder;
import com.epam.filmix.builder.impl.ArtistBuilder;
import com.epam.filmix.entity.Artist;
import com.epam.filmix.repository.AbstractRepository;
import com.epam.filmix.repository.TableFieldEnum;
import com.epam.filmix.repository.exception.RepositoryFilmRatingException;
import com.epam.filmix.repository.specification.Specification;

import java.util.List;
import java.util.Optional;

public class ArtistRepository extends AbstractRepository<Artist> {
    private TableFieldEnum tableEnum = TableFieldEnum.ARTIST;
    private Builder<Artist> artistBuilder = new ArtistBuilder();


    public Boolean save(Specification<Artist> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        return super.save(tableName, specification);
    }

    public List<Artist> queryForListResult(Specification<Artist> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        String tableFields = tableEnum.getTableFields();
        return super.queryForListResult(tableName, tableFields, artistBuilder, specification);
    }

    public Optional<Artist> queryForSingleResult(Specification<Artist> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        String tableFields = tableEnum.getTableFields();
        return super.queryForSingleResult(tableName, tableFields, artistBuilder, specification);
    }


    public Boolean update(Specification<Artist> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        return super.update(tableName, specification);
    }


    public Boolean remove(Specification<Artist> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        return super.remove(tableName, specification);
    }
}
