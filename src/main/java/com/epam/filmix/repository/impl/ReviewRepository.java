package com.epam.filmix.repository.impl;

import com.epam.filmix.builder.impl.ReviewBuilder;
import com.epam.filmix.entity.Review;
import com.epam.filmix.repository.AbstractRepository;
import com.epam.filmix.repository.TableFieldEnum;
import com.epam.filmix.repository.exception.RepositoryFilmRatingException;
import com.epam.filmix.repository.specification.Specification;

import java.util.List;
import java.util.Optional;

public class ReviewRepository extends AbstractRepository<Review> {
    private TableFieldEnum tableEnum = TableFieldEnum.REVIEW;
    private ReviewBuilder reviewBuilder = new ReviewBuilder();

    public Boolean save(Specification<Review> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        return super.save(tableName, specification);
    }

    public List<Review> queryForListResult(Specification<Review> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        String tableFields = tableEnum.getTableFields();
        return super.queryForListResult(tableName, tableFields, reviewBuilder, specification);
    }

    public Optional<Review> queryForSingleResult(Specification<Review> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        String tableFields = tableEnum.getTableFields();
        return super.queryForSingleResult(tableName, tableFields, reviewBuilder, specification);
    }


    public Boolean update(Specification<Review> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        return super.update(tableName, specification);
    }


    public Boolean remove(Specification<Review> specification) throws RepositoryFilmRatingException {
        String tableName = tableEnum.getTableName();
        return super.remove(tableName, specification);
    }
}