package com.epam.filmix.repository.specification.impl.update;

import com.epam.filmix.entity.Artist;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class UpdateArtistSpec implements Specification<Artist> {
    private List<Object> parameters = new ArrayList<>();

    public UpdateArtistSpec(Artist artist) {
        parameters.add(artist.getName());
        parameters.add(artist.getSurname());
        parameters.add(artist.getBirthday());
        parameters.add(artist.getBiography());
        parameters.add(artist.getId());
    }

    @Override
    public String toSQL() {
        return " name=?, surname=?, birthday=?, biography = ?  where id=?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
