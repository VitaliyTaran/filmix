package com.epam.filmix.repository.specification.impl.find;

import com.epam.filmix.entity.Film;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class FindFilmByType implements Specification<Film> {
    private List<Object> parameters = new ArrayList<>();

    public FindFilmByType(String type) {
        parameters.add(type);
    }

    @Override
    public String toSQL() {
        return "where type=?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
