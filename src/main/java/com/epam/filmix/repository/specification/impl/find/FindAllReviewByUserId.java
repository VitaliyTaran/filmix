package com.epam.filmix.repository.specification.impl.find;

import com.epam.filmix.entity.Review;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class FindAllReviewByUserId implements Specification<Review> {
    private List<Object> parameters = new ArrayList<>();

    public FindAllReviewByUserId(Long userId) {
        parameters.add(userId);
    }

    @Override
    public String toSQL() {
        return "where user_id = ?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
