package com.epam.filmix.repository.specification.impl.find;

import com.epam.filmix.entity.Role;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class FindRoleByFilmAndArtistIdSpec implements Specification<Role> {
    private List<Object> parameters = new ArrayList<>();

    public FindRoleByFilmAndArtistIdSpec(long filmId, long artistId) {
        parameters.add(filmId);
        parameters.add(artistId);
    }

    @Override
    public String toSQL() {
        return "where artist_film.film_id = ? and artist_film.artist_id = ?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
