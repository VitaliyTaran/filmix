package com.epam.filmix.repository.specification.impl.update;

import com.epam.filmix.entity.Review;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class UpdateReviewSpec implements Specification<Review> {
    private List<Object> parameters = new ArrayList<>();

    public UpdateReviewSpec(Review review) {
        parameters.add(review.getDiscription());
        parameters.add(review.getMark());
        parameters.add(review.getDate());
        parameters.add(review.getStatus());
        parameters.add(review.getUser().getId());
        parameters.add(review.getFilm().getId());
    }

    @Override
    public String toSQL() {
        return " review=?, mark=?, data_time=?  status = ? where user_id=? and film_id=?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
