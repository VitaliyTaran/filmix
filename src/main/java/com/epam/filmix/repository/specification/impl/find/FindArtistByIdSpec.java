package com.epam.filmix.repository.specification.impl.find;

import com.epam.filmix.entity.Artist;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class FindArtistByIdSpec implements Specification<Artist> {
    private List<Object> parameters = new ArrayList<>();

    public FindArtistByIdSpec(Long artistId) {
        parameters.add(artistId);
    }

    @Override
    public String toSQL() {
        return "where artist.id=?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
