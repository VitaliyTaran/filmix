package com.epam.filmix.repository.specification.impl.find;

import com.epam.filmix.entity.Review;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class FindAllReviewByFilmId implements Specification<Review> {
    private List<Object> parameters = new ArrayList<>();

    public FindAllReviewByFilmId(Long filmId) {
        parameters.add(filmId);
    }

    @Override
    public String toSQL() {
        return "where film_id = ?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
