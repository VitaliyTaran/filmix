package com.epam.filmix.repository.specification.impl.find;

import com.epam.filmix.entity.Film;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class FindFilmByTitleSpec implements Specification<Film> {
    private List<Object> parameters = new ArrayList<>();

    public FindFilmByTitleSpec(String title) {
        parameters.add(title);
    }

    @Override
    public String toSQL() {
        return "where film.title = ?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
