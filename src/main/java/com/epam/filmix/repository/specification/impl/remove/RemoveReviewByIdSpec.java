package com.epam.filmix.repository.specification.impl.remove;

import com.epam.filmix.entity.Review;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class RemoveReviewByIdSpec implements Specification<Review> {
    private List<Object> parameters = new ArrayList<>();

    public RemoveReviewByIdSpec(long id) {
        parameters.add(id);
    }

    @Override
    public String toSQL() {
        return "where id = ?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
