package com.epam.filmix.repository.specification.impl.update;

import com.epam.filmix.entity.Artist;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class UpdateRoleArtistInFilmSpec implements Specification<Artist> {
    private List<Object> parameters = new ArrayList<>();

    public UpdateRoleArtistInFilmSpec(Long filmId, Long artistId, String role) {
        parameters.add(filmId);
        parameters.add(artistId);
        parameters.add(role);
    }

    @Override
    public String toSQL() {
        return " role =? where artist_id=? and film_id";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
