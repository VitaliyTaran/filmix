package com.epam.filmix.repository.specification.impl.create;


import com.epam.filmix.entity.Review;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class CreateReviewSpec implements Specification<Review> {
    private List<Object> parameters = new ArrayList<>();

    public CreateReviewSpec(Review review) {
        parameters.add(review.getUser().getId());
        parameters.add(review.getFilm().getId());
        parameters.add(review.getDiscription());
        parameters.add(review.getMark());
        parameters.add(review.getDate());
        parameters.add(review.getStatus());
    }

    @Override
    public String toSQL() {
        return "(user_id,film_id,review,mark,data_time,status) values (?,?,?,?,?,?)";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
