package com.epam.filmix.repository.specification;

import java.util.List;

public interface Specification<T> {
    String toSQL();

    List<Object> parameters();
}
