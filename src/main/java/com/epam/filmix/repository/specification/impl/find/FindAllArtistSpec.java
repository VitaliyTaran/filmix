package com.epam.filmix.repository.specification.impl.find;

import com.epam.filmix.entity.Artist;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class FindAllArtistSpec implements Specification<Artist> {
    private List<Object> parameters = new ArrayList<>();

    @Override
    public String toSQL() {
        return "";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
