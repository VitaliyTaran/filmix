package com.epam.filmix.repository.specification.impl.find;

import com.epam.filmix.entity.Film;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class FindFilmById implements Specification<Film> {
    private List<Object> parameters = new ArrayList<>();

    public FindFilmById(Long filmId) {
        parameters.add(filmId);
    }

    @Override
    public String toSQL() {
        return "where id=?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}