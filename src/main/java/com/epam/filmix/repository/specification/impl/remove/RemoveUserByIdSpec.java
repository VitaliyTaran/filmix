package com.epam.filmix.repository.specification.impl.remove;


import com.epam.filmix.entity.User;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class RemoveUserByIdSpec implements Specification<User> {
    private List<Object> parameters = new ArrayList<>();

    public RemoveUserByIdSpec(Long userId) {
        parameters.add(userId);
    }

    @Override

    public String toSQL() {
        return "where id = ?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
