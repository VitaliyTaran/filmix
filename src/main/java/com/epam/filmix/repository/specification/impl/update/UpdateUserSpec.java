package com.epam.filmix.repository.specification.impl.update;

import com.epam.filmix.entity.User;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class UpdateUserSpec implements Specification<User> {
    private List<Object> parameters = new ArrayList<>();

    public UpdateUserSpec(User user) {
        parameters.add(user.getName());
        parameters.add(user.getSurname());
        parameters.add(user.getLogin());
        parameters.add(user.getPassword());
        parameters.add(user.getAdmin());
        parameters.add(user.getRating());
        parameters.add(user.getBlocked());
        parameters.add(user.getId());
    }

    @Override
    public String toSQL() {
        return " name=?, surname=?, login=?, password=?, is_admin=?, rating=?, is_blocked=? where id=?;";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
