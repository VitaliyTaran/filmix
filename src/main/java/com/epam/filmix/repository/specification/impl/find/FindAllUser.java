package com.epam.filmix.repository.specification.impl.find;


import com.epam.filmix.entity.User;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class FindAllUser implements Specification<User> {
    private List<Object> parameters = new ArrayList<>();

    @Override
    public String toSQL() {
        return "";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
