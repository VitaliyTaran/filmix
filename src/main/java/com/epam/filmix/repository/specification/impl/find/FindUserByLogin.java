package com.epam.filmix.repository.specification.impl.find;


import com.epam.filmix.entity.User;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class FindUserByLogin implements Specification<User> {
    private List<Object> parameters = new ArrayList<>();

    public FindUserByLogin(String login) {
        parameters.add(login);
    }

    @Override
    public String toSQL() {
        return " where login = ?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }


}
