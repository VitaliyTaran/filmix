package com.epam.filmix.repository.specification.impl.remove;

import com.epam.filmix.entity.Role;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class RemoveArtistFromAllFilmsByArtistIdSpec implements Specification<Role> {
    private List<Object> parameters = new ArrayList<>();

    public RemoveArtistFromAllFilmsByArtistIdSpec(long artistId) {
        parameters.add(artistId);
    }

    @Override
    public String toSQL() {
        return "where artist_id=?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
