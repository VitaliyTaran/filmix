package com.epam.filmix.repository.specification.impl.remove;


import com.epam.filmix.entity.Role;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class RemoveRoleByFilmIdSpec implements Specification<Role> {
    private List<Object> parameters = new ArrayList<>();

    public RemoveRoleByFilmIdSpec(long filmId) {
        parameters.add(filmId);
    }

    @Override
    public String toSQL() {
        return " where film_id = ?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
