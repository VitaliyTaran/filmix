package com.epam.filmix.repository.specification.impl.remove;

import com.epam.filmix.entity.Role;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class RemoveArtistFromFilmByIdSpec implements Specification<Role> {
    private List<Object> parameters = new ArrayList<>();

    public RemoveArtistFromFilmByIdSpec(Long filmId, Long artistId) {
        parameters.add(filmId);
        parameters.add(artistId);
    }

    @Override
    public String toSQL() {
        return "where film_id=? and artist_id=?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
