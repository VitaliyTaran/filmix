package com.epam.filmix.repository.specification.impl.find;


import com.epam.filmix.entity.User;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class FindAllBannedUser implements Specification<User> {

    private List<Object> parameters;

    public FindAllBannedUser() {
        parameters = new ArrayList<>();
    }

    @Override
    public String toSQL() {
        return "where is_blocked = 1";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
