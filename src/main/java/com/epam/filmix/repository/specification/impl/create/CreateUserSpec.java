package com.epam.filmix.repository.specification.impl.create;


import com.epam.filmix.entity.User;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class CreateUserSpec implements Specification<User> {
    private List<Object> parameters = new ArrayList<>();

    public CreateUserSpec(User user) {
        parameters.add(user.getName());
        parameters.add(user.getSurname());
        parameters.add(user.getLogin());
        parameters.add(user.getPassword());
        parameters.add(user.getAdmin());
        parameters.add(user.getRating());
        parameters.add(user.getBlocked());
    }

    @Override
    public String toSQL() {
        return "(name, surname, login, password, is_admin, rating, is_blocked) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
