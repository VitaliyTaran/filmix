package com.epam.filmix.repository.specification.impl.create;

import com.epam.filmix.entity.Artist;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class CreateArtistSpec implements Specification<Artist> {
    private List<Object> parameters = new ArrayList<>();

    public CreateArtistSpec(Artist artist) {
        parameters.add(artist.getName());
        parameters.add(artist.getSurname());
        parameters.add(artist.getBirthday());
        parameters.add(artist.getBiography());
    }

    @Override
    public String toSQL() {
        return "(name,surname,birthday,biography) values (?,?,?,?)";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
