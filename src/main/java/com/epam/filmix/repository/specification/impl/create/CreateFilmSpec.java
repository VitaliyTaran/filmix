package com.epam.filmix.repository.specification.impl.create;


import com.epam.filmix.entity.Film;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class CreateFilmSpec implements Specification<Film> {
    private List<Object> parameters = new ArrayList<>();

    public CreateFilmSpec(Film film) {
        parameters.add(film.getTitle());
        parameters.add(film.getDiscription());
        parameters.add(film.getGenre());
        parameters.add(film.getCountry());
        parameters.add(film.getReleaseDate());
    }

    @Override
    public String toSQL() {
        return "(title,discription,genre,country,release_date) values (?,?,?,?,?)";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
