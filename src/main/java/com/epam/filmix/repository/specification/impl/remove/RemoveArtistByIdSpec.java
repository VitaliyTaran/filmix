package com.epam.filmix.repository.specification.impl.remove;



import com.epam.filmix.entity.Artist;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class RemoveArtistByIdSpec implements Specification<Artist> {
    private List<Object> parameters = new ArrayList<>();

    public RemoveArtistByIdSpec(Long artist_id) {
        parameters.add(artist_id);
    }
    @Override
    public String toSQL() {
        return " where id = ?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
