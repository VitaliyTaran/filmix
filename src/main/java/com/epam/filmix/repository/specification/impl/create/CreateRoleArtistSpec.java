package com.epam.filmix.repository.specification.impl.create;

import com.epam.filmix.entity.Role;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class CreateRoleArtistSpec implements Specification<Role> {
    private List<Object> parameters = new ArrayList<>();

    public CreateRoleArtistSpec(Long filmId, Long artistId, String role) {
        parameters.add(artistId);
        parameters.add(filmId);
        parameters.add(role);
    }

    @Override
    public String toSQL() {
        return "(artist_id,film_id,role) values (?,?,?)";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
