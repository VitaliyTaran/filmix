package com.epam.filmix.repository.specification.impl.update;


import com.epam.filmix.entity.Film;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class UpdateFilmSpecification implements Specification<Film> {
    private List<Object> parameters = new ArrayList<>();

    public UpdateFilmSpecification(Film film) {
        parameters.add(film.getTitle());
        parameters.add(film.getDiscription());
        parameters.add(film.getGenre());
        parameters.add(film.getCountry());
        parameters.add(film.getReleaseDate());
        parameters.add(film.getId());
    }

    @Override
    public String toSQL() {
        return " title=?, discription=?, genre=?, country=?, release_date=? where id=?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
