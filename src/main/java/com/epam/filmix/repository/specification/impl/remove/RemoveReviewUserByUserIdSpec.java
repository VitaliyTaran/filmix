package com.epam.filmix.repository.specification.impl.remove;



import com.epam.filmix.entity.User;
import com.epam.filmix.repository.specification.Specification;

import java.util.List;

public class RemoveReviewUserByUserIdSpec implements Specification<User> {
    @Override
    public String toSQL() {
        return "where user_id = ?";
    }

    @Override
    public List<Object> parameters() {
        return null;
    }
}
