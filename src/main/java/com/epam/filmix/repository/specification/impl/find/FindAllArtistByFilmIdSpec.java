package com.epam.filmix.repository.specification.impl.find;

import com.epam.filmix.entity.Artist;
import com.epam.filmix.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class FindAllArtistByFilmIdSpec implements Specification<Artist> {
    private List<Object> parameters = new ArrayList<>();

    public FindAllArtistByFilmIdSpec(long filmId) {
        parameters.add(filmId);
    }

    @Override
    public String toSQL() {
        return "join artist_film on artist.id = artist_film.artist_id where artist_film.film_id = ?";
    }

    @Override
    public List<Object> parameters() {
        return parameters;
    }
}
