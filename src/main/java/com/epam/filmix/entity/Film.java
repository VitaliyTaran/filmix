package com.epam.filmix.entity;

import java.time.LocalDate;
import java.util.List;

public class Film implements Identifable {
    private long id;
    private String title;
    private String discription;
    private String genre;
    private String country;
    private LocalDate releaseDate;
    private String type;
    private double avgMark;
    private List<Artist> artists;
    private List<Review> reviews;

    public Film() {
    }

    public Film(Long id, String title, String discription, String genre, String country, LocalDate releaseDate, String type) {
        this.id = id;
        this.title = title;
        this.discription = discription;
        this.genre = genre;
        this.country = country;
        this.releaseDate = releaseDate;
        this.type = type;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAvgMark() {
        return avgMark;
    }

    public void setAvgMark(double avgMark) {
        this.avgMark = avgMark;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Film film = (Film) o;

        if (id != film.id) return false;
        if (Double.compare(film.avgMark, avgMark) != 0) return false;
        if (title != null ? !title.equals(film.title) : film.title != null) return false;
        if (discription != null ? !discription.equals(film.discription) : film.discription != null) return false;
        if (genre != null ? !genre.equals(film.genre) : film.genre != null) return false;
        if (country != null ? !country.equals(film.country) : film.country != null) return false;
        if (releaseDate != null ? !releaseDate.equals(film.releaseDate) : film.releaseDate != null) return false;
        if (type != null ? !type.equals(film.type) : film.type != null) return false;
        if (artists != null ? !artists.equals(film.artists) : film.artists != null) return false;
        return reviews != null ? reviews.equals(film.reviews) : film.reviews == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (discription != null ? discription.hashCode() : 0);
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (releaseDate != null ? releaseDate.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        temp = Double.doubleToLongBits(avgMark);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (artists != null ? artists.hashCode() : 0);
        result = 31 * result + (reviews != null ? reviews.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Film{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", discription='" + discription + '\'' +
                ", genre='" + genre + '\'' +
                ", country='" + country + '\'' +
                ", releaseDate=" + releaseDate +
                ", type='" + type + '\'' +
                ", avgMark=" + avgMark +
                ", artists=" + artists +
                ", reviews=" + reviews +
                '}';
    }
}
