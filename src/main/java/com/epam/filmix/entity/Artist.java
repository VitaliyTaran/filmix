package com.epam.filmix.entity;

import java.time.LocalDate;

public class Artist implements Identifable {
    private long id;
    private String name;
    private String surname;
    private LocalDate birthday;
    private String biography;
    private Role role;

    public Artist() {
    }

    public Artist(String name, String surname, LocalDate birthday, String biography) {
        this.name = name;
        this.surname = surname;
        this.biography = biography;
        this.birthday = birthday;
    }

    public Artist(long id, String name, String surname, LocalDate birthday, String biography) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.biography = biography;
        this.birthday = birthday;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Artist artist = (Artist) o;

        if (id != artist.id) return false;
        if (name != null ? !name.equals(artist.name) : artist.name != null) return false;
        if (surname != null ? !surname.equals(artist.surname) : artist.surname != null) return false;
        if (biography != null ? !biography.equals(artist.biography) : artist.biography != null) return false;
        if (birthday != null ? !birthday.equals(artist.birthday) : artist.birthday != null) return false;
        return role != null ? role.equals(artist.role) : artist.role == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (biography != null ? biography.hashCode() : 0);
        result = 31 * result + (birthday != null ? birthday.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Artist{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", biography='" + biography + '\'' +
                ", birthday=" + birthday +
                ", role=" + role +
                '}';
    }
}