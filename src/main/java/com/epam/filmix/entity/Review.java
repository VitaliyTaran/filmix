package com.epam.filmix.entity;

import java.time.LocalDate;


public class Review implements Identifable {
    private long id;
    private Film film;
    private User user;
    private String discription;
    private double mark;
    private LocalDate date;
    private String status;


    public Review() {
    }

    public Review(long id, Film film, User user, String discription, double mark, LocalDate date, String status) {
        this.id = id;
        this.film = film;
        this.user = user;
        this.discription = discription;
        this.mark = mark;
        this.date = date;
        this.status = status;
    }

    public Review(Film film, User user, String discription, double mark, LocalDate date, String status) {
        this.film = film;
        this.user = user;
        this.discription = discription;
        this.mark = mark;
        this.date = date;
        this.status = status;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public double getMark() {
        return mark;
    }

    public void setMark(double mark) {
        this.mark = mark;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Review review = (Review) o;

        if (id != review.id) return false;
        if (Double.compare(review.mark, mark) != 0) return false;
        if (film != null ? !film.equals(review.film) : review.film != null) return false;
        if (user != null ? !user.equals(review.user) : review.user != null) return false;
        if (discription != null ? !discription.equals(review.discription) : review.discription != null) return false;
        if (date != null ? !date.equals(review.date) : review.date != null) return false;
        return status != null ? status.equals(review.status) : review.status == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (film != null ? film.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (discription != null ? discription.hashCode() : 0);
        temp = Double.doubleToLongBits(mark);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + id +
                ", film=" + film +
                ", user=" + user +
                ", discription='" + discription + '\'' +
                ", mark=" + mark +
                ", date=" + date +
                ", status='" + status + '\'' +
                '}';
    }
}
