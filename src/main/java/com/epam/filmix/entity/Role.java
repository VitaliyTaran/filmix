package com.epam.filmix.entity;

public class Role implements Identifable {

    private long artistId;
    private long filmId;
    private String name;


    public Role(long artistId, long filmId, String name) {
        this.artistId = artistId;
        this.filmId = filmId;
        this.name = name;
    }

    @Override
    public long getId() {
        return artistId;
    }

    @Override
    public void setId(long artistId) {
        this.artistId = artistId;
    }

    public long getFilmId() {
        return filmId;
    }

    public void setFilmId(long filmId) {
        this.filmId = filmId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (artistId != role.artistId) return false;
        if (filmId != role.filmId) return false;
        return name != null ? name.equals(role.name) : role.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (artistId ^ (artistId >>> 32));
        result = 31 * result + (int) (filmId ^ (filmId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Role{" +
                "artistId=" + artistId +
                ", filmId=" + filmId +
                ", name='" + name + '\'' +
                '}';
    }
}
