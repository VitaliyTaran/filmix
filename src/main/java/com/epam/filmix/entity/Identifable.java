package com.epam.filmix.entity;

public interface Identifable {
    long getId();
    void setId(long id);
}
