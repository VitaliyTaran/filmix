package com.epam.filmix.command;

import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.exception.CommandFilmixException;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CommandFactory {
    private static CommandFactory instance;
    private static AtomicBoolean initialized = new AtomicBoolean(false);
    private static Lock lock = new ReentrantLock();

    private CommandFactory() {
    }

    public static CommandFactory getInstance() {
        if (!initialized.get()) {
            try {
                lock.lock();
                if (!initialized.get()) {
                    instance = new CommandFactory();
                    initialized.set(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public Command initCommand(HttpServletRequest request) throws CommandFilmixException {
        String command = request.getParameter(CommandConstant.COMMAND);
        if (command != null && CommandType.isExistCommand(command.toUpperCase())) {
            CommandType type = CommandType.valueOf(command.toUpperCase());
            return type.getCommand();
        } else {
            throw new CommandFilmixException(command + CommandConstant.NOT_EXIST_COMMAND_MESSAGE);
        }
    }
}
