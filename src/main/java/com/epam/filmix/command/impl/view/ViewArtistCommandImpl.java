package com.epam.filmix.command.impl.view;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.constant.PageEnum;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.entity.Artist;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.ArtistService;
import com.epam.filmix.logic.service.impl.ArtistServiceImpl;

import javax.servlet.http.HttpServletRequest;

public class ViewArtistCommandImpl implements Command {

    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        try {
            ArtistService artistService = new ArtistServiceImpl();
            Artist artist = artistService.artistByID(request.getParameter(CommandConstant.ID));
            request.setAttribute(CommandConstant.ARTIST, artist);
            return new CommandResult(CommandResult.ResponseType.FORWARD, PageEnum.ARTIST.getPagePath());
        } catch (LogicFilmixException e) {
            throw new CommandFilmixException(e);
        }

    }
}
