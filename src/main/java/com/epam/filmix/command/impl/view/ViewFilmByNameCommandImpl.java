package com.epam.filmix.command.impl.view;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.constant.PageEnum;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.FilmService;
import com.epam.filmix.logic.service.impl.FilmServiceImpl;

import javax.servlet.http.HttpServletRequest;

public class ViewFilmByNameCommandImpl implements Command {

    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        try {
            String search = request.getParameter(CommandConstant.SEARCH);
            FilmService filmService = new FilmServiceImpl();
            request.setAttribute(CommandConstant.FILMS, filmService.filmByTitle(search));
            return new CommandResult(CommandResult.ResponseType.FORWARD, PageEnum.FILMS.getPagePath());
        } catch (LogicFilmixException e) {
            throw new CommandFilmixException(e);
        }
    }
}
