package com.epam.filmix.command.impl.util;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.constant.PageEnum;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.entity.User;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.UserService;
import com.epam.filmix.logic.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

public class LoginCommandImpl implements Command {
    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        try {
            UserService userService = new UserServiceImpl();
            String login = request.getParameter(CommandConstant.LOGIN);
            String password = request.getParameter(CommandConstant.PASSWORD);
            Optional<User> user = userService.login(login, password);
            HttpSession session = request.getSession();
            if (user.isPresent()) {
                session.setAttribute(CommandConstant.USER_SESSION, user.get());
                session.setAttribute(CommandConstant.AUTORISATION_ERROR, null);
            } else {
                session.setAttribute(CommandConstant.AUTORISATION_ERROR, true);
            }
            return new CommandResult(CommandResult.ResponseType.FORWARD, CommandConstant.VIEW_ALL_FILM);
        } catch (LogicFilmixException e) {
            throw new CommandFilmixException(e);
        }
    }
}
