package com.epam.filmix.command.impl.save;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.entity.Artist;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.ArtistService;
import com.epam.filmix.logic.service.impl.ArtistServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

public class SaveArtistCommandImpl implements Command {
    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        ArtistService artistService = new ArtistServiceImpl();
        String id = request.getParameter(CommandConstant.ID);
        String name = request.getParameter(CommandConstant.NAME);
        String surname = request.getParameter(CommandConstant.SURNAME);
        String biography = request.getParameter(CommandConstant.BIOGRAPHY);
        LocalDate birthday = LocalDate.parse(request.getParameter(CommandConstant.BIRTHDAY));
        Artist artist;
        if (!id.equals("")) {
            artist = new Artist(Long.valueOf(id), name, surname, birthday, biography);
        } else {
            artist = new Artist(name, surname, birthday, biography);
        }
        try {
            artistService.saveArtist(artist);
        } catch (LogicFilmixException e) {
            throw new CommandFilmixException(e);
        }
        return new CommandResult(CommandResult.ResponseType.REDIRECT, CommandConstant.VIEW_ARTIST + id);
    }
}
