package com.epam.filmix.command.impl.view;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.PageEnum;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.logic.content.CommandResult;

import javax.servlet.http.HttpServletRequest;

public class ViewCreateFilmPageCommandImpl implements Command {

    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        return new CommandResult(CommandResult.ResponseType.FORWARD, PageEnum.CREATE_FILM.getPagePath());
    }
}
