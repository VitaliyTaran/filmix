package com.epam.filmix.command.impl.view;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.constant.PageEnum;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.UserService;
import com.epam.filmix.logic.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;

public class ViewAllUserCommandImpl implements Command {
    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        try {
            UserService userService = new UserServiceImpl();
            request.setAttribute(CommandConstant.USERS, userService.findAllUser());
            return new CommandResult(CommandResult.ResponseType.FORWARD, PageEnum.ALL_USER.getPagePath());
        } catch (LogicFilmixException e) {
            throw new CommandFilmixException(e);
        }
    }
}
