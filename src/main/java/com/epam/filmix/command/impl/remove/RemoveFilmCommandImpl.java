package com.epam.filmix.command.impl.remove;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.FilmService;
import com.epam.filmix.logic.service.impl.FilmServiceImpl;

import javax.servlet.http.HttpServletRequest;

public class RemoveFilmCommandImpl implements Command {
    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        try {
            FilmService filmService = new FilmServiceImpl();
            filmService.removeFilmById(request.getParameter(CommandConstant.ID));
        } catch (LogicFilmixException e) {
            e.printStackTrace();
        }
        return new CommandResult(CommandResult.ResponseType.REDIRECT, CommandConstant.VIEW_ALL_FILM);
    }
}
