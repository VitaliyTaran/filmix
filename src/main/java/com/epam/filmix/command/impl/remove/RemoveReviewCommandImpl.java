package com.epam.filmix.command.impl.remove;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.ReviewService;
import com.epam.filmix.logic.service.impl.ReviewServiceImpl;

import javax.servlet.http.HttpServletRequest;

public class RemoveReviewCommandImpl implements Command {
    private ReviewService reviewService = new ReviewServiceImpl();

    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        Long filmId = Long.valueOf(request.getParameter(CommandConstant.FILM_ID));
        try {
            reviewService.removeById(request.getParameter(CommandConstant.ID));
        } catch (LogicFilmixException e) {
            e.printStackTrace();
        }
        return new CommandResult(CommandResult.ResponseType.REDIRECT, CommandConstant.VIEW_FILM + filmId);
    }
}
