package com.epam.filmix.command.impl.save;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.entity.Film;
import com.epam.filmix.entity.Review;
import com.epam.filmix.entity.User;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.ReviewService;
import com.epam.filmix.logic.service.impl.ReviewServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class SaveReviewCommandImpl implements Command {

    private ReviewService reviewService = new ReviewServiceImpl();

    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        String time = request.getParameter(CommandConstant.DATE);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate date = LocalDate.parse(time, dateTimeFormatter);
        Film film = new Film();
        Long filmID = Long.valueOf(request.getParameter(CommandConstant.FILM_ID));
        film.setId(filmID);
        User user = new User();
        Long userID = Long.valueOf(request.getParameter(CommandConstant.USER_ID));
        user.setId(userID);
        String reviewDiscription = request.getParameter(CommandConstant.REVIEW);
        Integer mark = Integer.valueOf(request.getParameter(CommandConstant.MARK));
        String status = request.getParameter(CommandConstant.STATUS);
        try {
            Review review = new Review(film, user, reviewDiscription, mark, date, status);
            reviewService.saveReview(review);
        } catch (LogicFilmixException e) {
            throw new CommandFilmixException(e);
        }
        return new CommandResult(CommandResult.ResponseType.REDIRECT, CommandConstant.VIEW_FILM + filmID);
    }
}
