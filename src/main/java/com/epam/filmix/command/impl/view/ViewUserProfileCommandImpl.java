package com.epam.filmix.command.impl.view;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.constant.PageEnum;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.entity.User;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.UserService;
import com.epam.filmix.logic.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public class ViewUserProfileCommandImpl implements Command {
    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        try {
            UserService userService = new UserServiceImpl();
            String userId = request.getParameter(CommandConstant.ID);
            User user = userService.findById(userId);
            request.setAttribute(CommandConstant.USER, user);
            return new CommandResult(CommandResult.ResponseType.FORWARD, PageEnum.PROFILE.getPagePath());
        } catch (LogicFilmixException e) {
            throw new CommandFilmixException(e);
        }
    }
}
