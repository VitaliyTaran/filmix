package com.epam.filmix.command.impl.util;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.constant.PageEnum;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.logic.content.CommandResult;

import javax.servlet.http.HttpServletRequest;

public class ForwardCommandImpl implements Command {
    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        String page = request.getParameter(CommandConstant.PAGE);
        if (PageEnum.isExistPage(page)) {
            PageEnum pageEnum = PageEnum.valueOf(page.toUpperCase());
            return new CommandResult(CommandResult.ResponseType.FORWARD, pageEnum.getPagePath());
        } else {
            return new CommandResult(CommandResult.ResponseType.FORWARD, page);
        }

    }
}
