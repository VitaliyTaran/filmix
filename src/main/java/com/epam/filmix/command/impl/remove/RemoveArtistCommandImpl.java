package com.epam.filmix.command.impl.remove;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.ArtistService;
import com.epam.filmix.logic.service.impl.ArtistServiceImpl;

import javax.servlet.http.HttpServletRequest;

public class RemoveArtistCommandImpl implements Command {
    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        try {
            ArtistService artistService = new ArtistServiceImpl();
            artistService.removeArtist(request.getParameter(CommandConstant.ID));
        } catch (LogicFilmixException e) {
            throw new CommandFilmixException(e);
        }
        return new CommandResult(CommandResult.ResponseType.REDIRECT, CommandConstant.VIEW_ALL_FILM);
    }
}
