package com.epam.filmix.command.impl.save;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.entity.Film;
import com.epam.filmix.entity.User;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.FilmService;
import com.epam.filmix.logic.service.impl.FilmServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;

public class SaveFilmCommandImpl implements Command {
    private FilmService filmService = new FilmServiceImpl();

    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        HttpSession session = request.getSession();
        Film film = new Film();
        User userSession = (User) session.getAttribute(CommandConstant.USER_SESSION);
        String filmId = request.getParameter(CommandConstant.ID);
        if (userSession.getAdmin().equals(true)) {
            String page;
            if (!filmId.equals("")) {
                film.setId(Long.valueOf(filmId));
                page = CommandConstant.VIEW_FILM + filmId;
            } else {
                page = CommandConstant.VIEW_ALL_FILM;
            }
            film.setTitle(request.getParameter(CommandConstant.TITLE));
            film.setDiscription(request.getParameter(CommandConstant.DISCRIPTION));
            film.setGenre(request.getParameter(CommandConstant.GENRE));
            film.setCountry(request.getParameter(CommandConstant.COUNTRY));
            film.setReleaseDate(LocalDate.parse(request.getParameter(CommandConstant.RELEASE_DATE)));
            film.setType(request.getParameter(CommandConstant.TYPE));
            try {
                filmService.saveFilm(film);
            } catch (LogicFilmixException e) {
                throw new CommandFilmixException(e);
            }
            return new CommandResult(CommandResult.ResponseType.REDIRECT, page);
        } else {
            // TODO: 20.01.2019 redo this path
            String someErrorPage = "";
            return new CommandResult(CommandResult.ResponseType.REDIRECT, someErrorPage);
        }
    }
}
