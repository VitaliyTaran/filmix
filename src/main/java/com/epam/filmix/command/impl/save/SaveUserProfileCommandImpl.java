package com.epam.filmix.command.impl.save;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.constant.PageEnum;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.entity.User;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.UserService;
import com.epam.filmix.logic.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SaveUserProfileCommandImpl implements Command {
    private UserService userService = new UserServiceImpl();

    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        HttpSession session = request.getSession();
        User user = new User();
        User userSession = (User) session.getAttribute(CommandConstant.USER_SESSION);
        String userId = request.getParameter(CommandConstant.ID);
        user.setName(request.getParameter(CommandConstant.NAME));
        user.setSurname(request.getParameter(CommandConstant.SURNAME));
        user.setLogin(request.getParameter(CommandConstant.LOGIN));
        user.setPassword(request.getParameter(CommandConstant.PASSWORD));
        user.setAdmin(Boolean.valueOf(request.getParameter(CommandConstant.ADMIN)));
        user.setBlocked(Boolean.valueOf(request.getParameter(CommandConstant.BLOCK)));
        user.setRating(Integer.valueOf(request.getParameter(CommandConstant.RATING)));
        String page;
        if (!userId.equals("")) {
            user.setId(Long.valueOf(userId));
            page = CommandConstant.VIEW_USER_PROFILE + userId;
            if (userSession.getId() == Long.valueOf(userId) || userSession.getAdmin().equals(true)) {
                try {
                    userService.saveUser(user);
                } catch (LogicFilmixException e) {
                    throw new CommandFilmixException(e);
                }
            }
        } else {
            if (request.getParameter(CommandConstant.PASSWORD).equals(request.getParameter(CommandConstant.CHECK_PASSWORD))) {
                page = CommandConstant.VIEW_ALL_FILM;
                try {
                    userService.saveUser(user);
                } catch (LogicFilmixException e) {
                    throw new CommandFilmixException(e);
                }
            } else {
                page = PageEnum.ERROR_PAGE.getPagePath();
            }
        }
        return new CommandResult(CommandResult.ResponseType.REDIRECT, page);

    }
}
