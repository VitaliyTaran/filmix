package com.epam.filmix.command.impl.util;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.constant.PageEnum;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.logic.content.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LogoutCommandImpl implements Command {
    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        HttpSession session = request.getSession();
        session.setAttribute(CommandConstant.USER_SESSION, null);
        return new CommandResult(CommandResult.ResponseType.REDIRECT, CommandConstant.VIEW_ALL_FILM);
    }
}