package com.epam.filmix.command.impl.save;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.ArtistService;
import com.epam.filmix.logic.service.impl.ArtistServiceImpl;

import javax.servlet.http.HttpServletRequest;

public class AddArtistFilmCommandImpl implements Command {
    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        ArtistService artistService = new ArtistServiceImpl();
        String filmId = request.getParameter(CommandConstant.FILM_ID);
        String artistId = request.getParameter(CommandConstant.ARTIST_ID);
        String role = request.getParameter(CommandConstant.ROLE);
        try {
            artistService.addArtistToFilm(filmId, artistId, role);
        } catch (LogicFilmixException e) {
            throw new CommandFilmixException(e);
        }
        return new CommandResult(CommandResult.ResponseType.REDIRECT, CommandConstant.VIEW_FILM + filmId);
    }
}
