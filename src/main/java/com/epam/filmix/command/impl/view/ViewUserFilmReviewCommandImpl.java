package com.epam.filmix.command.impl.view;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.constant.PageEnum;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.entity.User;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.ReviewService;
import com.epam.filmix.logic.service.impl.ReviewServiceImpl;

import javax.servlet.http.HttpServletRequest;

public class ViewUserFilmReviewCommandImpl implements Command {
    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        try {
            ReviewService reviewService = new ReviewServiceImpl();
            User userSession = (User) request.getSession().getAttribute(CommandConstant.USER_SESSION);
            String userId = String.valueOf(userSession.getId());
            request.setAttribute(CommandConstant.REVIEWS, reviewService.allReviewByUserId(userId));
        } catch (LogicFilmixException e) {
            throw new CommandFilmixException(e);
        }
        return new CommandResult(CommandResult.ResponseType.FORWARD, PageEnum.USER_REVIEWS.getPagePath());
    }
}
