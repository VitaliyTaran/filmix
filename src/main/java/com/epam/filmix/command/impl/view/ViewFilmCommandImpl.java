package com.epam.filmix.command.impl.view;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.constant.PageEnum;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.ArtistService;
import com.epam.filmix.logic.service.FilmService;
import com.epam.filmix.logic.service.ReviewService;
import com.epam.filmix.logic.service.impl.ArtistServiceImpl;
import com.epam.filmix.logic.service.impl.FilmServiceImpl;
import com.epam.filmix.logic.service.impl.ReviewServiceImpl;

import javax.servlet.http.HttpServletRequest;

public class ViewFilmCommandImpl implements Command {
    private FilmService filmService = new FilmServiceImpl();
    private ReviewService reviewService = new ReviewServiceImpl();
    private ArtistService artistService = new ArtistServiceImpl();

    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        try {
            String id = request.getParameter(CommandConstant.ID);
            request.setAttribute(CommandConstant.FILM, filmService.filmById(id));
            request.setAttribute(CommandConstant.REVIEWS, reviewService.allReviewsByFilmId(id));
            request.setAttribute(CommandConstant.ARTISTS, artistService.allArtist());
            return new CommandResult(CommandResult.ResponseType.FORWARD, PageEnum.FILM.getPagePath());
        } catch (LogicFilmixException e) {
            throw new CommandFilmixException(e);
        }
    }
}
