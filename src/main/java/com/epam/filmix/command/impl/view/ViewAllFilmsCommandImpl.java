package com.epam.filmix.command.impl.view;

import com.epam.filmix.command.Command;
import com.epam.filmix.command.constant.CommandConstant;
import com.epam.filmix.command.constant.PageEnum;
import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.logic.content.CommandResult;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.FilmService;
import com.epam.filmix.logic.service.impl.FilmServiceImpl;

import javax.servlet.http.HttpServletRequest;

public class ViewAllFilmsCommandImpl implements Command {
    private FilmService filmService = new FilmServiceImpl();

    @Override
    public CommandResult execute(HttpServletRequest request) throws CommandFilmixException {
        try {
            if (request.getParameter(CommandConstant.TYPE) != null) {
                String type = request.getParameter(CommandConstant.TYPE).toUpperCase();
                request.setAttribute(CommandConstant.FILMS, filmService.filmByType(type));
            } else {
                request.setAttribute(CommandConstant.FILMS, filmService.allFilm());
            }
            return new CommandResult(CommandResult.ResponseType.FORWARD, PageEnum.FILMS.getPagePath());
        } catch (LogicFilmixException e) {
            throw new CommandFilmixException(e);
        }
    }
}
