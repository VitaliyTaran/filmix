package com.epam.filmix.command;

import com.epam.filmix.command.exception.CommandFilmixException;
import com.epam.filmix.logic.content.CommandResult;

import javax.servlet.http.HttpServletRequest;


@FunctionalInterface
public interface Command {
    CommandResult execute(HttpServletRequest request) throws CommandFilmixException;
}
