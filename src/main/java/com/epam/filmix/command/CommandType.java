package com.epam.filmix.command;

import com.epam.filmix.command.impl.remove.RemoveArtistCommandImpl;
import com.epam.filmix.command.impl.remove.RemoveArtistFromFilmCommandImpl;
import com.epam.filmix.command.impl.remove.RemoveFilmCommandImpl;
import com.epam.filmix.command.impl.remove.RemoveReviewCommandImpl;
import com.epam.filmix.command.impl.save.*;
import com.epam.filmix.command.impl.util.ForwardCommandImpl;
import com.epam.filmix.command.impl.util.LocaleCommandImpl;
import com.epam.filmix.command.impl.util.LoginCommandImpl;
import com.epam.filmix.command.impl.util.LogoutCommandImpl;
import com.epam.filmix.command.impl.view.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum CommandType {
    LOGIN(new LoginCommandImpl()),
    LOGOUT(new LogoutCommandImpl()),
    USER_PROFILE(new ViewUserProfileCommandImpl()),
    USER_ALL(new ViewAllUserCommandImpl()),
    FORWARD(new ForwardCommandImpl()),
    LOCALE(new LocaleCommandImpl()),
    SAVE_USER_PROFILE(new SaveUserProfileCommandImpl()),
    FILM_ALL(new ViewAllFilmsCommandImpl()),
    FILM(new ViewFilmCommandImpl()),
    SAVE_FILM(new SaveFilmCommandImpl()),
    SAVE_REVIEW(new SaveReviewCommandImpl()),
    REMOVE_REVIEW(new RemoveReviewCommandImpl()),
    VIEW_CREATE_FILM_PAGE(new ViewCreateFilmPageCommandImpl()),
    REMOVE_FILM(new RemoveFilmCommandImpl()),
    ARTIST(new ViewArtistCommandImpl()),
    SAVE_ARTIST(new SaveArtistCommandImpl()),
    USER_REVIEWS(new ViewUserFilmReviewCommandImpl()),
    SEARCH(new ViewFilmByNameCommandImpl()),
    ADD_ARTIST(new AddArtistFilmCommandImpl()),
    REMOVE_ARTIST_FILM(new RemoveArtistFromFilmCommandImpl()),
    REMOVE_ARTIST(new RemoveArtistCommandImpl());


    private Command command;

    CommandType(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }

    public static boolean isExistCommand(String command) {
        List<CommandType> commands = Arrays.stream(CommandType.values())
                .filter(localCommand -> command.equals(localCommand.name()))
                .collect(Collectors.toList());
        return !commands.isEmpty();
    }
}
// TODO: 21.01.2019 allUsersPage 
// TODO: 21.01.2019 myCommentsPage 
// TODO: 21.01.2019 menu for creating entities 
// TODO: 21.01.2019 remove main page and change it to all films 
