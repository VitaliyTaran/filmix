package com.epam.filmix.command.constant;


public class CommandConstant {
    public static final String FORWARD = "/controller?command=forward&page=";
    public static final String VIEW_USER_PROFILE = "/controller?command=user_profile&id=";
    public static final String VIEW_FILM = "/controller?command=film&id=";
    public static final String VIEW_ALL_FILM = "/controller?command=film_all";
    public static final String VIEW_ARTIST = "/controller?command=artist&id=";
    public static final String PAGE = "page";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String CHECK_PASSWORD = "checkPassword";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String ADMIN = "admin";
    public static final String BLOCK = "block";
    public static final String RATING = "rating";
    public static final String ID = "id";
    public static final String STATUS = "status";
    public static final String USER = "user";
    public static final String LOCALE = "locale";
    public static final String COMMAND = "command";
    public static final String NOT_EXIST_COMMAND_MESSAGE = " command isn't exist";
    public static final String USER_SESSION = "userSession";
    public static final String AUTORISATION_ERROR = "autorisation error";
    public static final String FILM = "film";
    public static final String FILMS = "films";
    public static final String VALUE = "value";
    public static final String TYPE = "type";
    public static final String TITLE = "title";
    public static final String DISCRIPTION = "discription";
    public static final String GENRE = "genre";
    public static final String COUNTRY = "country";
    public static final String RELEASE_DATE = "releaseDate";
    public static final String USER_ID = "userId";
    public static final String FILM_ID = "filmId";
    public static final String DATE = "date";
    public static final String MARK = "mark";
    public static final String REVIEW = "review";
    public static final String REVIEWS = "reviews";
    public static final String USERS = "users";
    public static final String ARTIST = "artist";
    public static final String FILM_IMAGE = "filmImage";
    public static final String BIOGRAPHY = "biography";
    public static final String BIRTHDAY = "birthday";
    public static final String SEARCH = "search";
    public static final String ARTISTS = "artists";
    public static final String ARTIST_ID = "artistId";
    public static final String ROLE = "role";

}
