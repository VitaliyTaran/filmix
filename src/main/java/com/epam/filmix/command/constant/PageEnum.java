package com.epam.filmix.command.constant;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum PageEnum {
    ALL_USER("/WEB-INF/pages/users.jsp"),
    PROFILE("/WEB-INF/pages/user_page.jsp"),
    FILMS("/WEB-INF/pages/films.jsp"),
    FILM("/WEB-INF/pages/film_page.jsp"),
    CREATE_FILM("/WEB-INF/pages/create_film.jsp"),
    CREATE_ARTIST("/WEB-INF/pages/create_artist.jsp"),
    ARTIST("/WEB-INF/pages/artist_page.jsp"),
    USER_REVIEWS("/WEB-INF/pages/user_reviews.jsp"),
    ERROR_PAGE("/WEB-INF/pages/error_page.jsp"),
    CREATE_USER("/WEB-INF/pages/create_user.jsp");


    private String pagePath;

    PageEnum(String pagePath) {
        this.pagePath = pagePath;
    }

    public String getPagePath() {
        return pagePath;
    }

    public static boolean isExistPage(String page) {
        String tempPage = page.toUpperCase();
        List<PageEnum> commands = Arrays.stream(PageEnum.values())
                .filter(localCommand -> tempPage.equals(localCommand.name()))
                .collect(Collectors.toList());
        return !commands.isEmpty();
    }
}
