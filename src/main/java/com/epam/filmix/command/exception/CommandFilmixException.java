package com.epam.filmix.command.exception;

/**
 * The Class CommandFilmixException.
 */
public class CommandFilmixException extends Exception {

    /**
     * Instantiates a new command lite job exception.
     */
    public CommandFilmixException() {
    }

    /**
     * Instantiates a new command lite job exception.
     *
     * @param message the message
     */
    public CommandFilmixException(String message) {
        super(message);
    }

    /**
     * Instantiates a new command lite job exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public CommandFilmixException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new command lite job exception.
     *
     * @param cause the cause
     */
    public CommandFilmixException(Throwable cause) {
        super(cause);
    }
}
