package com.epam.filmix.logic.constant;

public class LogicConstant {
    public static final String ENTITY_NOT_EXIST_WITH_ID_MESSAGE = "The entity doesn't exist with id = ";
    public static final String ENTITY_NOT_EXIST_MESSAGE = "The entity doesn't exist.";
    public static final String INCORRECT_PASSWORD_MESSAGE = "Incorrect password.";
}
