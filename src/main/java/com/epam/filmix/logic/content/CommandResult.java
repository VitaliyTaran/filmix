package com.epam.filmix.logic.content;

public class CommandResult {

    public enum ResponseType {
        FORWARD,
        REDIRECT
    }
    private ResponseType responseType;
    private String page;
    public CommandResult() {
    }
    public CommandResult(ResponseType responseType, String page) {
        this.responseType = responseType;
        this.page = page;
    }

    public ResponseType getResponseType() {
        return responseType;
    }
    public void setResponseType(ResponseType responseType) {
        this.responseType = responseType;
    }
    public String getPage() {
        return page;
    }
    public void setPage(String page) {
        this.page = page;
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( !(o instanceof CommandResult) ) return false;

        CommandResult that = (CommandResult) o;

        if ( responseType != that.responseType ) return false;
        return page != null ? page.equals(that.page) : that.page == null;
    }

    @Override
    public int hashCode() {
        int result = responseType != null ? responseType.hashCode() : 0;
        result = 31 * result + (page != null ? page.hashCode() : 0);
        return result;
    }
}
