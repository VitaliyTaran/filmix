package com.epam.filmix.logic.exception;

public class LogicFilmixException extends Exception {

    public LogicFilmixException() {
    }

    public LogicFilmixException(String message) {
        super(message);
    }

    public LogicFilmixException(String message, Throwable cause) {
        super(message, cause);
    }

    public LogicFilmixException(Throwable cause) {
        super(cause);
    }
}
