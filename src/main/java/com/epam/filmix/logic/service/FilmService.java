package com.epam.filmix.logic.service;

import com.epam.filmix.entity.Film;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.repository.impl.FilmRepository;

import java.util.List;

public interface FilmService {
    List<Film> allFilm() throws LogicFilmixException;

    List<Film> filmByType(String type) throws LogicFilmixException;

    Film filmById(String id) throws LogicFilmixException;

    void saveFilm(Film film) throws LogicFilmixException;

    void removeFilmById(String id) throws LogicFilmixException;

    List<Film> filmByTitle(String title) throws LogicFilmixException;

    void setFilmRepository(FilmRepository filmRepository);
}
