package com.epam.filmix.logic.service;

import com.epam.filmix.entity.Review;
import com.epam.filmix.logic.exception.LogicFilmixException;

import java.util.List;

public interface ReviewService {
    void saveReview(Review review) throws LogicFilmixException;

    List<Review> allReviewsByFilmId(String filmId) throws LogicFilmixException;

    void removeById(String reviewId) throws LogicFilmixException;

    List<Review> allReviewByUserId(String userId)throws LogicFilmixException;
}
