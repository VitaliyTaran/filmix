package com.epam.filmix.logic.service.impl;

import com.epam.filmix.entity.Artist;
import com.epam.filmix.entity.Role;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.ArtistService;
import com.epam.filmix.repository.exception.RepositoryFilmRatingException;
import com.epam.filmix.repository.impl.ArtistRepository;
import com.epam.filmix.repository.impl.RoleRepository;
import com.epam.filmix.repository.specification.Specification;
import com.epam.filmix.repository.specification.impl.create.CreateArtistSpec;
import com.epam.filmix.repository.specification.impl.create.CreateRoleArtistSpec;
import com.epam.filmix.repository.specification.impl.find.FindAllArtistSpec;
import com.epam.filmix.repository.specification.impl.find.FindArtistByIdSpec;
import com.epam.filmix.repository.specification.impl.remove.RemoveArtistByIdSpec;
import com.epam.filmix.repository.specification.impl.remove.RemoveArtistFromAllFilmsByArtistIdSpec;
import com.epam.filmix.repository.specification.impl.remove.RemoveArtistFromFilmByIdSpec;
import com.epam.filmix.repository.specification.impl.update.UpdateArtistSpec;
import com.epam.filmix.util.OptionalExtractor;
import com.epam.filmix.validator.PositiveLongValidator;

import java.util.List;
import java.util.Optional;

public class ArtistServiceImpl implements ArtistService {
    private ArtistRepository artistRepository = new ArtistRepository();
    private RoleRepository roleRepository = new RoleRepository();

     @Override
    public void setArtistRepository(ArtistRepository artistRepository) {
        this.artistRepository = artistRepository;
    }

    @Override
    public void setRoleRepository(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<Artist> allArtist() throws LogicFilmixException {
        try {
            FindAllArtistSpec findAllArtistSpec = new FindAllArtistSpec();
            return artistRepository.queryForListResult(findAllArtistSpec);
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }

    @Override
    public Artist artistByID(String id) throws LogicFilmixException {
        try {
            long longId = PositiveLongValidator.parseStringToLong(id);
            FindArtistByIdSpec findArtistByIdSpec = new FindArtistByIdSpec(longId);
            Optional<Artist> artistOptional = artistRepository.queryForSingleResult(findArtistByIdSpec);
            return (Artist) OptionalExtractor.extractEntityFromOptional(artistOptional, longId);
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }

    @Override
    public boolean saveArtist(Artist artist) throws LogicFilmixException {
        try {
            if (artist.getId() == 0) {
                CreateArtistSpec addArtistSpec = new CreateArtistSpec(artist);
                return artistRepository.save(addArtistSpec);
            } else {
                UpdateArtistSpec specification = new UpdateArtistSpec(artist);
                return artistRepository.update(specification);
            }
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }

    @Override
    public void addArtistToFilm(String filmId, String artistId, String role) throws LogicFilmixException {
        try {
            long longFilmId = PositiveLongValidator.parseStringToLong(filmId);
            long longArtistId = PositiveLongValidator.parseStringToLong(artistId);
            Specification<Role> specification = new CreateRoleArtistSpec(longFilmId, longArtistId, role);
            roleRepository.save(specification);
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }


    @Override
    public void removeArtist(String id) throws LogicFilmixException {
        long longId = PositiveLongValidator.parseStringToLong(id);
        Specification<Role> roleSpecification = new RemoveArtistFromAllFilmsByArtistIdSpec(longId);
        Specification<Artist> artistSpecification = new RemoveArtistByIdSpec(longId);
        try {
            roleRepository.remove(roleSpecification);
            artistRepository.remove(artistSpecification);
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }

    @Override
    public void removeArtistFromFilm(String filmId, String artistId) throws LogicFilmixException {
        try {
            long longFilmId = PositiveLongValidator.parseStringToLong(filmId);
            long longArtistId = PositiveLongValidator.parseStringToLong(artistId);
            Specification<Role> specification = new RemoveArtistFromFilmByIdSpec(longFilmId, longArtistId);
            roleRepository.remove(specification);
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }
}
