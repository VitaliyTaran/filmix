package com.epam.filmix.logic.service;

import com.epam.filmix.entity.User;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.repository.impl.UserRepository;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<User> login(String login, String password) throws LogicFilmixException;

    User findById(String id) throws LogicFilmixException;

    List<User> findAllUser() throws LogicFilmixException;

    void saveUser(User user) throws LogicFilmixException;

    void setUserRepository(UserRepository userRepository);
}
