package com.epam.filmix.logic.service;

import com.epam.filmix.entity.Artist;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.repository.impl.ArtistRepository;
import com.epam.filmix.repository.impl.RoleRepository;

import java.util.List;
import java.util.Optional;

public interface ArtistService {
    List<Artist> allArtist() throws LogicFilmixException;

    Artist artistByID(String id) throws LogicFilmixException;

    boolean saveArtist(Artist artist) throws LogicFilmixException;

    void addArtistToFilm(String filmId, String artistId, String role) throws LogicFilmixException;


    void removeArtist(String id) throws LogicFilmixException;

    void removeArtistFromFilm(String filmId, String artistId) throws LogicFilmixException;

    void setArtistRepository(ArtistRepository artistRepository);

    void setRoleRepository(RoleRepository roleRepository);
}
