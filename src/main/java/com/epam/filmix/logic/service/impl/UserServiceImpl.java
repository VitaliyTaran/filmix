package com.epam.filmix.logic.service.impl;

import com.epam.filmix.entity.User;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.UserService;
import com.epam.filmix.repository.exception.RepositoryFilmRatingException;
import com.epam.filmix.repository.impl.UserRepository;
import com.epam.filmix.repository.specification.Specification;
import com.epam.filmix.repository.specification.impl.create.CreateUserSpec;
import com.epam.filmix.repository.specification.impl.find.FindAllUser;
import com.epam.filmix.repository.specification.impl.find.FindUserByIdSpec;
import com.epam.filmix.repository.specification.impl.find.FindUserByLogin;
import com.epam.filmix.repository.specification.impl.update.UpdateUserSpec;
import com.epam.filmix.util.OptionalExtractor;
import com.epam.filmix.validator.PositiveLongValidator;

import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements UserService {
    private UserRepository userRepository = new UserRepository();

    @Override
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<User> login(String login, String password) throws LogicFilmixException {
        Optional<User> userOptional;
        try {
            FindUserByLogin findUserByLogin = new FindUserByLogin(login);
            userOptional = userRepository.queryForSingleResult(findUserByLogin);
            if (userOptional.isPresent()) {
                User user = userOptional.get();
                if (password.equals(user.getPassword())) {
                    return userOptional;
                }
            }
            return Optional.empty();
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }

    @Override
    public List<User> findAllUser() throws LogicFilmixException {
        try {
            FindAllUser findAllUserSpec = new FindAllUser();
            return userRepository.queryForListResult(findAllUserSpec);
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }

    @Override
    public void saveUser(User user) throws LogicFilmixException {
        try {
            if (user.getId() == 0) {
                Specification<User> specification = new CreateUserSpec(user);
                userRepository.save(specification);
            } else {
                Specification<User> specification = new UpdateUserSpec(user);
                userRepository.update(specification);
            }

        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }


    @Override
    public User findById(String id) throws LogicFilmixException {
        try {
            long longId = PositiveLongValidator.parseStringToLong(id);
            FindUserByIdSpec findUserByLoginSpec = new FindUserByIdSpec(longId);
            Optional<User> user = userRepository.queryForSingleResult(findUserByLoginSpec);
            return (User) OptionalExtractor.extractEntityFromOptional(user, longId);
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }
}
