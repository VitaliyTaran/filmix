package com.epam.filmix.logic.service.impl;

import com.epam.filmix.entity.Artist;
import com.epam.filmix.entity.Film;
import com.epam.filmix.entity.Review;
import com.epam.filmix.entity.Role;
import com.epam.filmix.logic.constant.LogicConstant;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.FilmService;
import com.epam.filmix.repository.exception.RepositoryFilmRatingException;
import com.epam.filmix.repository.impl.ArtistRepository;
import com.epam.filmix.repository.impl.FilmRepository;
import com.epam.filmix.repository.impl.ReviewRepository;
import com.epam.filmix.repository.impl.RoleRepository;
import com.epam.filmix.repository.specification.Specification;
import com.epam.filmix.repository.specification.impl.find.*;
import com.epam.filmix.repository.specification.impl.remove.RemoveFilmByIdSpec;
import com.epam.filmix.repository.specification.impl.remove.RemoveReviewByFilmIdSpec;
import com.epam.filmix.repository.specification.impl.remove.RemoveRoleByFilmIdSpec;
import com.epam.filmix.repository.specification.impl.create.CreateFilmSpec;
import com.epam.filmix.repository.specification.impl.update.UpdateFilmSpecification;
import com.epam.filmix.util.OptionalExtractor;
import com.epam.filmix.validator.PositiveLongValidator;

import java.util.List;
import java.util.Optional;

public class FilmServiceImpl implements FilmService {
    private FilmRepository filmRepository = new FilmRepository();

    @Override
    public void setFilmRepository(FilmRepository filmRepository) {
        this.filmRepository = filmRepository;
    }

    @Override
    public List<Film> allFilm() throws LogicFilmixException {
        try {
            FindAllFilm specification = new FindAllFilm();
            return filmRepository.queryForListResult(specification);
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }

    @Override
    public List<Film> filmByType(String type) throws LogicFilmixException {
        try {
            FindFilmByType specification = new FindFilmByType(type);
            return filmRepository.queryForListResult(specification);
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }

    @Override
    public Film filmById(String filmId) throws LogicFilmixException {
        try {
            double sum = 0;
            long longFilmId = PositiveLongValidator.parseStringToLong(filmId);
            FindFilmById specification = new FindFilmById(longFilmId);
            Optional<Film> result = filmRepository.queryForSingleResult(specification);
            Film film = (Film) OptionalExtractor.extractEntityFromOptional(result, longFilmId);

            ReviewRepository reviewRepository = new ReviewRepository();
            FindAllReviewByFilmId findAllReviewByFilmIdSpecification = new FindAllReviewByFilmId(longFilmId);
            List<Review> reviews = reviewRepository.queryForListResult(findAllReviewByFilmIdSpecification);

            for (Review review : reviews) {
                sum += review.getMark();
            }

            double avrMark = sum / reviews.size();
            ArtistRepository artistRepository = new ArtistRepository();
            FindAllArtistByFilmIdSpec artistSpecification = new FindAllArtistByFilmIdSpec(longFilmId);
            List<Artist> artists = artistRepository.queryForListResult(artistSpecification);
            RoleRepository roleRepository = new RoleRepository();

            for (Artist artist : artists) {
                FindRoleByFilmAndArtistIdSpec roleSpecification = new FindRoleByFilmAndArtistIdSpec(longFilmId, artist.getId());
                Optional<Role> role = roleRepository.queryForSingleResult(roleSpecification);
                role.ifPresent(artist::setRole);
            }

            film.setArtists(artists);
            film.setAvgMark(avrMark);
            return film;
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }

    @Override
    public void saveFilm(Film film) throws LogicFilmixException {
        try {
            if (film.getId() == 0) {
                CreateFilmSpec specification = new CreateFilmSpec(film);
                filmRepository.save(specification);
            } else {
                UpdateFilmSpecification specification = new UpdateFilmSpecification(film);
                filmRepository.update(specification);
            }
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }

    @Override
    public void removeFilmById(String filmId) throws LogicFilmixException {
        try {
            long longFilmId = PositiveLongValidator.parseStringToLong(filmId);
            ReviewRepository reviewRepository = new ReviewRepository();
            RemoveReviewByFilmIdSpec removeReviewByFilmIdSpec = new RemoveReviewByFilmIdSpec(longFilmId);
            reviewRepository.remove(removeReviewByFilmIdSpec);

            RoleRepository roleRepository = new RoleRepository();
            RemoveRoleByFilmIdSpec removeRoleByFilmIdSpec = new RemoveRoleByFilmIdSpec(longFilmId);
            roleRepository.remove(removeRoleByFilmIdSpec);

            RemoveFilmByIdSpec specification = new RemoveFilmByIdSpec(longFilmId);
            filmRepository.remove(specification);
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }

    @Override
    public List<Film> filmByTitle(String title) throws LogicFilmixException {
        try {
            Specification<Film> specification = new FindFilmByTitleSpec(title);
            return filmRepository.queryForListResult(specification);
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }
}