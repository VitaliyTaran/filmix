package com.epam.filmix.logic.service.impl;

import com.epam.filmix.entity.Film;
import com.epam.filmix.entity.Review;
import com.epam.filmix.entity.User;
import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.ReviewService;
import com.epam.filmix.repository.exception.RepositoryFilmRatingException;
import com.epam.filmix.repository.impl.FilmRepository;
import com.epam.filmix.repository.impl.ReviewRepository;
import com.epam.filmix.repository.impl.UserRepository;
import com.epam.filmix.repository.specification.Specification;
import com.epam.filmix.repository.specification.impl.find.FindAllReviewByUserId;
import com.epam.filmix.repository.specification.impl.create.CreateReviewSpec;
import com.epam.filmix.repository.specification.impl.find.FindAllReviewByFilmId;
import com.epam.filmix.repository.specification.impl.find.FindFilmById;
import com.epam.filmix.repository.specification.impl.find.FindUserByIdSpec;
import com.epam.filmix.repository.specification.impl.remove.RemoveReviewByIdSpec;
import com.epam.filmix.util.OptionalExtractor;
import com.epam.filmix.validator.PositiveLongValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ReviewServiceImpl implements ReviewService {
    private ReviewRepository reviewRepository = new ReviewRepository();

    @Override
    public void saveReview(Review review) throws LogicFilmixException {
        try {
            Specification<Review> specification = new CreateReviewSpec(review);
            reviewRepository.save(specification);
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }

    @Override
    public List<Review> allReviewsByFilmId(String filmId) throws LogicFilmixException {
        long longFilmId = PositiveLongValidator.parseStringToLong(filmId);
        Specification<Review> specification = new FindAllReviewByFilmId(longFilmId);
        return allReviewsBySomeId(specification);
    }

    private List<Review> allReviewsBySomeId(Specification<Review> reviewSpecification) throws LogicFilmixException {
        List<Review> result = new ArrayList<>();
        FilmRepository filmRepository = new FilmRepository();
        UserRepository userRepository = new UserRepository();
        try {

            List<Review> reviews = reviewRepository.queryForListResult(reviewSpecification);
            for (Review review : reviews) {
                User reviewUser = review.getUser();
                Film reviewFilm = review.getFilm();

                Specification<User> userSpecification = new FindUserByIdSpec(reviewUser.getId());
                Optional<User> optionalUser = userRepository.queryForSingleResult(userSpecification);
                User user = (User) OptionalExtractor.extractEntityFromOptional(optionalUser, reviewUser.getId());

                Specification<Film> filmSpecification = new FindFilmById(reviewFilm.getId());
                Optional<Film> optionalFilm = filmRepository.queryForSingleResult(filmSpecification);
                Film film = (Film) OptionalExtractor.extractEntityFromOptional(optionalFilm, reviewFilm.getId());

                Review temp = new Review();
                temp.setId(review.getId());
                temp.setFilm(film);
                temp.setUser(user);
                temp.setDiscription(review.getDiscription());
                temp.setMark(review.getMark());
                temp.setDate(review.getDate());
                temp.setStatus(review.getStatus());

                result.add(temp);
            }
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
        return result;
    }

    @Override
    public void removeById(String reviewId) throws LogicFilmixException {
        try {
            long longReviewId = PositiveLongValidator.parseStringToLong(reviewId);
            Specification<Review> specification = new RemoveReviewByIdSpec(longReviewId);
            reviewRepository.remove(specification);
        } catch (RepositoryFilmRatingException e) {
            throw new LogicFilmixException(e);
        }
    }

    @Override
    public List<Review> allReviewByUserId(String userId) throws LogicFilmixException {
        long longUserId = PositiveLongValidator.parseStringToLong(userId);
        Specification<Review> specification = new FindAllReviewByUserId(longUserId);
        return allReviewsBySomeId(specification);
    }
}
