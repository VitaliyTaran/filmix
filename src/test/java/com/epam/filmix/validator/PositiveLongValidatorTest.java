package com.epam.filmix.validator;

import com.epam.filmix.logic.exception.LogicFilmixException;
import org.junit.Test;

import static org.junit.Assert.*;

public class PositiveLongValidatorTest {

    @Test
    public void validate() throws Exception {
        assertTrue(PositiveLongValidator.validate("123"));
    }

    @Test
    public void validateIncorrectValue() throws Exception {
        assertFalse(PositiveLongValidator.validate("test"));
    }

    @Test
    public void parseStringToLong() throws Exception {
        assertEquals(PositiveLongValidator.parseStringToLong("123"), 123);
    }

    @Test(expected = LogicFilmixException.class)
    public void parseStringToLongWithIncorrectValue() throws Exception {
        PositiveLongValidator.parseStringToLong("asdf");
    }

}