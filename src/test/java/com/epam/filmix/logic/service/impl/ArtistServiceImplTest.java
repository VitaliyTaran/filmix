package com.epam.filmix.logic.service.impl;

import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.ArtistService;
import com.epam.filmix.repository.exception.RepositoryFilmRatingException;
import com.epam.filmix.repository.impl.ArtistRepository;
import com.epam.filmix.repository.specification.impl.find.FindAllArtistSpec;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ArtistServiceImplTest {
    private ArtistService artistService;
    private ArtistRepository artistRepository;

    @Before
    public void setUp() throws Exception {
        artistService = new ArtistServiceImpl();
        artistRepository = mock(ArtistRepository.class);
        artistService.setArtistRepository(artistRepository);
    }

    @Test
    public void allArtist() throws RepositoryFilmRatingException, LogicFilmixException {
        when(artistRepository.queryForListResult(new FindAllArtistSpec())).thenReturn(new ArrayList<>());
        assertEquals(artistService.allArtist(), new ArrayList<>());
    }
}