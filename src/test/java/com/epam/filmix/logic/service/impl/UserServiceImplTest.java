package com.epam.filmix.logic.service.impl;

import com.epam.filmix.logic.service.UserService;
import com.epam.filmix.repository.impl.UserRepository;
import com.epam.filmix.repository.specification.impl.find.FindAllUser;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceImplTest {
    private UserService userService;
    private UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
        userService = new UserServiceImpl();
        userRepository = mock(UserRepository.class);
        userService.setUserRepository(userRepository);
    }

    @Test
    public void findAllUser() throws Exception {
        when(userRepository.queryForListResult(new FindAllUser())).thenReturn(new ArrayList<>());
        assertEquals(userService.findAllUser(), new ArrayList<>());
    }

}