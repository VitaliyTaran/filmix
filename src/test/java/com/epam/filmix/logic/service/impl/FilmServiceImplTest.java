package com.epam.filmix.logic.service.impl;

import com.epam.filmix.logic.exception.LogicFilmixException;
import com.epam.filmix.logic.service.FilmService;
import com.epam.filmix.repository.exception.RepositoryFilmRatingException;
import com.epam.filmix.repository.impl.FilmRepository;
import com.epam.filmix.repository.specification.impl.find.FindAllArtistSpec;
import com.epam.filmix.repository.specification.impl.find.FindAllFilm;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FilmServiceImplTest {
    private FilmService filmService;
    private FilmRepository filmRepository;

    @Before
    public void setUp() throws Exception {
        filmService = new FilmServiceImpl();
        filmRepository = mock(FilmRepository.class);
        filmService.setFilmRepository(filmRepository);
    }

    @Test
    public void allFilms() throws RepositoryFilmRatingException, LogicFilmixException {
        when(filmRepository.queryForListResult(new FindAllFilm())).thenReturn(new ArrayList<>());
        assertEquals(filmService.allFilm(), new ArrayList<>());
    }
}